
#include <stdio.h>
#include <math.h>
#include "perspective.h"


void PrintMatrix(Matrix *M);
void InvertMatrix(Matrix *A, Matrix *B);
void MatrixMultiply(Matrix *A, Matrix *B, Matrix *C);


/*
 * SquareToQuad : map a unit square onto an arbitrary quadrilateral
 *   Map [0,0] -> p0
 *   Map [1,0] -> p1
 *   Map [1,1] -> p2
 *   Map [0,1] -> p3
 *
 */

void SquareToQuad(const Point2D p0, const Point2D p1, const Point2D p2, const Point2D p3, Matrix *A) {

  double dx1 = p1.x - p2.x;
  double dx2 = p3.x - p2.x;
  double dx3 = p0.x - p1.x + p2.x - p3.x;
  double dy1 = p1.y - p2.y;
  double dy2 = p3.y - p2.y;
  double dy3 = p0.y - p1.y + p2.y - p3.y;

  A->a33 = 1;

  if ((dx3 == 0) && (dy3 == 0)) {
    A->a11 = p1.x - p0.x;
    A->a21 = p2.x - p1.x;
    A->a31 = p0.x;
    A->a12 = p1.y - p0.y;
    A->a22 = p2.y - p1.y;
    A->a32 = p0.y;
    A->a13 = 0;
    A->a23 = 0;
  } else {
    A->a13 = ((dx3 * dy2) - (dy3 * dx2)) / ((dx1 * dy2) - (dy1 * dx2));
    A->a23 = ((dx1 * dy3) - (dy1 * dx3)) / ((dx1 * dy2) - (dy1 * dx2));
    A->a11 = p1.x - p0.x + (A->a13 * p1.x);
    A->a21 = p3.x - p0.x + (A->a23 * p3.x);
    A->a31 = p0.x;
    A->a12 = p1.y - p0.y + (A->a13 * p1.y);
    A->a22 = p3.y - p0.y + (A->a23 * p3.y);
    A->a32 = p0.y;
  }

// fprintf(stderr,"Points\n[%d,%d] [%d,%d] [%d,%d] [%d,%d]\n",p0.x,p0.y,p1.x,p1.y,p2.x,p2.y,p3.x,p3.y);
// fprintf(stderr,"Square to Quad Coeffs\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n",A->a11/A->a33,A->a21/A->a33,A->a31/A->a33,A->a12/A->a33,A->a22/A->a33,A->a32/A->a33,A->a13/A->a33,A->a23/A->a33);

}




/*
 * QuadToSquare : map an arbitrary quadrilateral onto a unit square
 *   Map p0 -> [0,0]
 *   Map p1 -> [1,0]
 *   Map p2 -> [1,1]
 *   Map p3 -> [0,1]
 *
 */

void QuadToSquare(Point2D p0, Point2D p1, Point2D p2, Point2D p3, Matrix *A) {

  Matrix M;
  SquareToQuad(p0, p1, p2, p3, &M);
  Adjoint(&M, A);

}


/*
 * Adjoint : calculate the adjoint of the src and return the value via dest
 *
 */

void Adjoint(Matrix *src, Matrix *dest) {

  dest->a11 = (src->a22 * src->a33) - (src->a32 * src->a23);
  dest->a12 = (src->a32 * src->a13) - (src->a12 * src->a33);
  dest->a13 = (src->a12 * src->a23) - (src->a22 * src->a13);
  dest->a21 = (src->a31 * src->a23) - (src->a21 * src->a33);
  dest->a22 = (src->a11 * src->a33) - (src->a31 * src->a13);
  dest->a23 = (src->a21 * src->a13) - (src->a11 * src->a23);
  dest->a31 = (src->a21 * src->a32) - (src->a31 * src->a22);
  dest->a32 = (src->a31 * src->a12) - (src->a11 * src->a32);
  dest->a33 = (src->a11 * src->a22) - (src->a21 * src->a12);
   
}


/*
 * Determinant : calculate the determinant of the Matrix M
 *
 */

double Determinant(Matrix *M) {

  return (M->a11 * M->a22 * M->a33) + (M->a12 * M->a23 * M->a31) + (M->a13 * M->a21 * M->a32)
          - (M->a13 * M->a22 * M->a31) - (M->a12 * M->a21 * M->a33) - (M->a11 * M->a23 * M->a32);

}


/*
 * MatrixMultiply : multiply two matrices (A and B) together to give result C
 *
 */

void MatrixMultiply(Matrix *A, Matrix *B, Matrix *C) {

  C->a11 = (A->a11 * B->a11) + (A->a12 * B->a21) + (A->a13 * B->a31);
  C->a12 = (A->a11 * B->a12) + (A->a12 * B->a22) + (A->a13 * B->a32);
  C->a13 = (A->a11 * B->a13) + (A->a12 * B->a23) + (A->a13 * B->a33);
  C->a21 = (A->a21 * B->a11) + (A->a22 * B->a21) + (A->a23 * B->a31);
  C->a22 = (A->a21 * B->a12) + (A->a22 * B->a22) + (A->a23 * B->a32);
  C->a23 = (A->a21 * B->a13) + (A->a22 * B->a23) + (A->a23 * B->a33);
  C->a31 = (A->a31 * B->a11) + (A->a32 * B->a21) + (A->a33 * B->a31);
  C->a32 = (A->a31 * B->a12) + (A->a32 * B->a22) + (A->a33 * B->a32);
  C->a33 = (A->a31 * B->a13) + (A->a32 * B->a23) + (A->a33 * B->a33);
   
}


/*
 * ScalarMultiply : multiply Matrix A by the scalar s to give result Matrix B
 *
 */

void ScalarMultiply(Matrix *A, double s, Matrix *B) {

  B->a11 = A->a11 * s;
  B->a12 = A->a12 * s;
  B->a13 = A->a13 * s;
  B->a21 = A->a21 * s;
  B->a22 = A->a22 * s;
  B->a23 = A->a23 * s;
  B->a31 = A->a31 * s;
  B->a32 = A->a32 * s;
  B->a33 = A->a33 * s;
   
}


/*
 * InvertMatrix : invert matrix A to give result matrix B
 *
 */

void InvertMatrix(Matrix *A, Matrix *B) {

  Matrix M;
  Adjoint(A,&M);
  ScalarMultiply(&M,Determinant(A),B);
   
}


/*
 * PerspectiveTransform : transform a pixel U in the reference polygon [u0,u1,u2,u3]
 *                        to a vector V in the observed polygon [v0,v1,v2,v3]
 *
 */

void PerspectiveTransform(Point2D U, Vector2D *V, 
                          Point2D u0, Point2D u1, Point2D u2, Point2D u3,
                          Point2D v0, Point2D v1, Point2D v2, Point2D v3) {

  Matrix QtoS, StoQ, A;
  
  QuadToSquare(u0,u1,u2,u3,&QtoS);
  SquareToQuad(v0,v1,v2,v3,&StoQ);
  MatrixMultiply(&QtoS,&StoQ,&A);
  V->setx(((A.a11 * U.x) + (A.a21 * U.y) + A.a31) / ((A.a13 * U.x) + (A.a23 * U.y) + A.a33));
  V->sety(((A.a12 * U.x) + (A.a22 * U.y) + A.a32) / ((A.a13 * U.x) + (A.a23 * U.y) + A.a33));

}



void PrintMatrix(Matrix *M){
  fprintf(stderr,"%.2f %.2f %.2f\n%.2f %.2f %.2f\n%.2f %.2f %.2f\n",
          M->a11,M->a12,M->a13,M->a21,M->a22,M->a23,M->a31,M->a32,M->a33);
}
  
