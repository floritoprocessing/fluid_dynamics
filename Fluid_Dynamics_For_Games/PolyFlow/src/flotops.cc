#include "pcm.h"
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>

using namespace std;

void print_usage()
    {
    printf("Usage: flotops <input .pcm file>\n");
    printf("               <flow vector scale>\n");
    printf("               <pixels between flow vectors>\n");
    printf("               [image size scale - default = 1.0]\n");
    printf("Still stuck? Try: flotops optflow.pcm 5 5 2\n\n");
    exit(1);
    }

int main(int argc,char **argv)
{
    // print header
    printf("flotops\n");
    printf("===========\n");
    printf("Converts a flow map (2D vector field) to a ps representation\n\n");

    // check input arguments
    if (argc != 4 && argc != 5) print_usage();
    char *input_filename = argv[1];
    double scale = atof(argv[2]);
    int skip = atoi(argv[3]);
    double image_scale = 1.0;
    if (argc == 5) image_scale = atof(argv[4]);

    // print out parameters
    printf("Flowmap:          %s\n",(char*)input_filename);
    printf("Vector Scale:     %f\n",scale);
    printf("Skip:             %d\n",skip);
    printf("Image size scale: %f\n",image_scale);

	PCM inflow(input_filename);

	string outfile = string(input_filename)+".ps";
	ofstream of(outfile.data());

	// output header
	of << "%!PS-Adobe-2.0 EPSF-2.0" << endl;
	of << "%%Title: " << outfile << endl;
	of << "%%Creator: flotops version 1.0 - by Brendan McCane" << endl;
	of << "%%BoundingBox: 0 0 " << inflow.width*image_scale << " " 
	   << inflow.height*image_scale << endl;
	of << "%%Pages: 1" << endl
	   << "%%DocumentFonts:" << endl
	   << "%%EndComments" << endl
	   << "%%EndProlog" << endl
	   << "%%Page: 1 1" << endl
	   << "/origstate save def" << endl
	   << "20 dict begin" << endl;

	// now output the needle map
	of << "newpath" << endl;
	for (int i=0; i<inflow.width; i+=skip)
		for (int j=0; j<inflow.height; j+=skip)
		{
			float x = i*image_scale;
			float y = (inflow.height-j-1)*image_scale;
			complex *c = inflow.Get(i, j);
			float x2 = inflow.Get(i, j)->r*scale;
			float y2 = -inflow.Get(i, j)->i*scale;
			of << x << " " << y << " 1.5 0 360 arc fill" << endl;
			// of << x << " " << y << " translate doACircle" << endl;
			of << x << " " << y << " moveto" << endl
			   << x2 << " " << y2 << " rlineto" << endl;
			of << "stroke" << endl << endl;
		}
	
	// now output the trailer
	of << "showpage" << endl
	   << "end" << endl
	   << "origstate restore" << endl
	   << "%%Trailer" << endl;
}
