
/*
 * pcm.h
 *
 * Author: Dion Crannitch (adapted from Dave Mason's complexMap class)
 * Last Modified : 22 June 1999
 *
 * Declarations for PCM class, a data structure containing a 2D vector
 * image, with encapsulated operations for reading and writing PCM files.
 *
 */

#ifndef __PCM__
#define __PCM__

#include <stdio.h>

typedef struct { 
  float r,i;
} complex;

class PCM {
  void CalcMax();
 public:
  int width,height;
  unsigned long pixels;
  float max;
  complex *image; 
  PCM(int w,int h);
  PCM(char *filename);
  complex *Get(int x, int y);
  void Set(int x, int y, complex c);
  void Load(char *filename);
  void Save(char *filename);
  ~PCM();
};

#endif


