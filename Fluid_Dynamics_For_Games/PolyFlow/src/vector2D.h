/*
 * vector2D.h
 *
 */

#ifndef _VECTOR2D_H_
#define _VECTOR2D_H_

class Vector2D {
  double x;
  double y;
public:
  Vector2D(double this_x, double this_y);
  Vector2D();
  double getx() {return x;};
  double gety() {return y;};
  void setx(double thisx) {x=thisx;};
  void sety(double thisy) {y=thisy;};
  void Set(double thisx, double thisy) {x=thisx;y=thisy;};
  double VectorLength();
};

double VectorAngle(Vector2D U, Vector2D V);
double DotProduct(Vector2D U, Vector2D V);

#endif 
