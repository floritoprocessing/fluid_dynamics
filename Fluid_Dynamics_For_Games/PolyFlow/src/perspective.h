
#ifndef _PERSPECTIVE_H_
#define _PERSPECTIVE_H_


#include "globals.h"
#include "vector2D.h"



/*
 * PerspectiveTransform : transform a pixel U in the reference polygon [u0,u1,u2,u3]
 *                        to a vector V in the observed polygon [v0,v1,v2,v3]
 *
 */

void PerspectiveTransform(Point2D U, Vector2D *V, 
                          Point2D u0, Point2D u1, Point2D u2, Point2D u3,
                          Point2D v0, Point2D v1, Point2D v2, Point2D v3);



/*
 * SquareToQuad : map a unit square onto an arbitrary quadrilateral
 *   Map [0,0] -> p0
 *   Map [1,0] -> p1
 *   Map [1,1] -> p2
 *   Map [0,1] -> p3
 *
 */

void SquareToQuad(Point2D p0, Point2D p1, Point2D p2, Point2D p3, Matrix *A);


/*
 * QuadToSquare : map arbitrary quadrilateral onto an a unit square
 *   Map p0 -> [0,0]
 *   Map p1 -> [1,0]
 *   Map p2 -> [1,1]
 *   Map p3 -> [0,1]
 *
 */

void SquareToQuad(Point2D p0, Point2D p1, Point2D p2, Point2D p3, Matrix *A);


/*
 * Adjoint : calculate the adjoint of the src and return the value via dest
 *
 */

void Adjoint(Matrix *src, Matrix *dest);







#endif
