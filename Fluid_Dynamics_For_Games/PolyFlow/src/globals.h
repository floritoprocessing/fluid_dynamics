
#ifndef _GLOBALS_H_
#define _GLOBALS_H_



#define MAXSTRING 100
#define G_WINDING_THRESHOLD 6
#define pi 3.141592654




struct Point2D_t {
  double x;
  double y;
};
typedef struct Point2D_t Point2D;


struct Matrix_t {

  double a11, a21, a31, a12, a22, a32, a13, a23, a33;

};
typedef struct Matrix_t Matrix;



#endif
