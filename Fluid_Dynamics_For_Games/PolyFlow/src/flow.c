/*
 * flow.c
 *
 * contains the functions used to calculate the flow for polygons
 * 
 *                 
 */

//-- Includes -----------------------------------------------------------

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "flow.h"
#include "vector2D.h"
#include "pcm.h"
#include "perspective.h"


//-- Defines ------------------------------------------------------------

#define SHOWPROGRESS


//-- Prototypes ---------------------------------------------------------

void ShowProgress (const char *message, int x, int y, int width, int height);


//-- Globals ------------------------------------------------------------

double gProgress;


//-- DoFlowCalculation --------------------------------------------------

/*
 * DoFlowCalculation calculates the flow for polygons over a series of images
 * Gets point data from the global tcl variables infoArray and pointArray
 *
 */

int DoFlowCalculation(ClientData clientData, Tcl_Interp *interp, 
	  int argc, const char *argv[]) {

  int startFrame, endFrame, frameCount;
  char varString[MAXSTRING];
  double d;
 
  strcpy(varString,"infoArray(frameCount)");
  int err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),&frameCount);
  if (argc < 3) {
    endFrame = frameCount - 1;  
  } else {
    endFrame = atoi(argv[2]);
  }

  if (argc < 2) {
    startFrame = 0;
  } else {
    startFrame = atoi(argv[1]);
  }
  //fprintf(stderr,"StartFrame %d EndFrame %d\n",startFrame,endFrame);


  // load up all the point information from infoArray and pointArray
  int polygonCount;
  strcpy(varString,"infoArray(polygonCount)");
  err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),&polygonCount);
  Point2D *thePoints[frameCount][polygonCount];
  int pointCounts[polygonCount];
  for (int frame=0;frame<frameCount;frame++) { 
    for (int polygon=0;polygon<polygonCount;polygon++) {
      sprintf(varString,"infoArray(polygon%d.pointCount)",polygon);
      err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),
             &pointCounts[polygon]);
      thePoints[frame][polygon] = (Point2D *) calloc (pointCounts[polygon],sizeof(Point2D));
      for (int point=0;point<pointCounts[polygon];point++) {
        sprintf(varString,"pointArray(%d,%d,%d,X)",frame,polygon,point);
        err = Tcl_GetDouble(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),
               &d);
        thePoints[frame][polygon][point].x=(int)d;
        //fprintf(stderr,"%s is %d\n",varString,thePoints[frame][polygon][point].x);
        sprintf(varString,"pointArray(%d,%d,%d,Y)",frame,polygon,point);
        err = Tcl_GetDouble(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),
               &d);
        thePoints[frame][polygon][point].y=(int)d;
        //fprintf(stderr,"%s is %d\n",varString,thePoints[frame][polygon][point].y);
      }
    }
  }

  // get the image dimensions and save path
  int imageWidth;
  strcpy(varString,"canvasWidth");
  err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),&imageWidth);
  int imageHeight;
  strcpy(varString,"canvasHeight");
  err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),&imageHeight);
  char savePath[MAXSTRING];
  Tcl_GetVar(interp,savePath,TCL_GLOBAL_ONLY);
  int doBackgroundFlow;
  strcpy(varString,"infoArray(BackgroundFlow)");
  err = Tcl_GetInt(interp,Tcl_GetVar(interp,varString,TCL_GLOBAL_ONLY),&doBackgroundFlow);

  //  fprintf(stderr,"doBackgroundFlow is %d\n",doBackgroundFlow);
  //  fprintf(stderr,"savePath is %s\n",savePath);


  // set up a PCM for output
  PCM *pcm = new PCM(imageWidth,imageHeight);
  
  // for each pixel of each frame (except the last one), 
  // determine which polygon it is in (if any) and calculate the flow
  for (int frame=0;frame<(frameCount-1);frame++) {
    // fprintf(stderr,"Frame %d\n",frame);
    complex flowVector;
    flowVector.r = 0;
    flowVector.i = 0;

#ifdef SHOWPROGRESS
    fprintf(stderr,"Calculating flow from frame %d to frame %d\n",frame,frame+1);
    gProgress = 0;
#endif

    // first we need to initialise the complex map to zero
    for (int y=0;y<imageHeight;y++)
      for (int x=0;x<imageWidth;x++)
	pcm->Set(x,y,flowVector);
    

    // then for every pixel in the frame, if it is inside a polygon
    // calculate the corresponding pixel in the previous frame and
    // determine the flow vector
    // NB if two (or more) polygons occupy the same pixel then the
    // last polygon checked is the one used

    Vector2D transformedPoint;
    Point2D thisPoint;
    int flowSet;

    for (int y=0;y<imageHeight;y++)
      for (int x=0;x<imageWidth;x++) {
	flowSet = 0;
	thisPoint.x = x;
	thisPoint.y = y;
        for (int polygon=0;polygon<polygonCount;polygon++) {

	  // if we're inside a polygon use it's points for the transformation
          if ((pointCounts[polygon]==4) && 
	      InsidePolygon(x,y,pointCounts[polygon],thePoints[frame][polygon])) {

            // transform the point forward to the same polygon in the next image

            // fprintf(stderr,"Calling PerspectiveTransform with [%d,%d]\n",thisPoint.x,thisPoint.y);
            PerspectiveTransform(thisPoint,&transformedPoint,
				 thePoints[frame][polygon][0],thePoints[frame][polygon][1],
				 thePoints[frame][polygon][2],thePoints[frame][polygon][3],
				 thePoints[frame+1][polygon][0],thePoints[frame+1][polygon][1],
				 thePoints[frame+1][polygon][2],thePoints[frame+1][polygon][3]);
            // fprintf(stderr,"PerspectiveTransform returned [%d,%d]\n",transformedPoint.x,transformedPoint.y);
	    flowSet = 1;
	  }
	}

	if (!flowSet && doBackgroundFlow) {

	  // we are not inside any of the polygons so have hit the background
	  // polygon zero should be a polygon in the background/floor plane
	  // therefore use polygon zero to calculate the flow

	  // fprintf(stderr,"Calling PerspectiveTransform with [%f,%f]\n",thisPoint.x,thisPoint.y);
	  PerspectiveTransform(thisPoint,&transformedPoint,
			       thePoints[frame][0][0],thePoints[frame][0][1],
			       thePoints[frame][0][2],thePoints[frame][0][3],
			       thePoints[frame+1][0][0],thePoints[frame+1][0][1],
			       thePoints[frame+1][0][2],thePoints[frame+1][0][3]);
	  // fprintf(stderr,"PerspectiveTransform returned [%f,%f]\n",transformedPoint.getx(),transformedPoint.gety());
	  flowSet = 1;
	}

	if (flowSet) {
	  // the flow vector for the current point is (transformedPoint - thisPoint)
	  flowVector.r = transformedPoint.getx() - thisPoint.x;
	  flowVector.i = transformedPoint.gety() - thisPoint.y;
	} else {
	  flowVector.r = 0;
	  flowVector.i = 0;
	}
	pcm->Set(x,y,flowVector);	

#ifdef SHOWPROGRESS
	ShowProgress("Progress",x,y,imageWidth,imageHeight);
#endif
      }

    // write the output file
    // output file for frame k to k+1 is called flow.(k+1).pcm
    char saveFilename[MAXSTRING];
    sprintf(saveFilename,"%sflow.%03d.pcm",savePath,frame+1);
#ifdef SHOWPROGRESS
    fprintf(stderr,"Writing to file %s\n",saveFilename);
#endif
    pcm->Save(saveFilename);
  }
  return TCL_OK;
}


//-- AbsVal -------------------------------------------------------------


double AbsVal (double d){
  if (d<0) {
    return -d;
  } else {
    return d;
  }
}


//-- InsidePolygon ------------------------------------------------------


int InsidePolygon(int x, int y, int pointCount, Point2D *points) {

  double winding = 0;
  Vector2D U;
  Vector2D V;

  if (((x == points[pointCount-1].x) && (y == points[pointCount-1].y))
               ||((x == points[0].x) && (y == points[0].y))) {
    winding = 2 * pi;
  } else {
    U.Set(points[pointCount-1].x - x,points[pointCount-1].y - y);
    V.Set(points[0].x - x,points[0].y - y);
    winding = VectorAngle(U,V);
  }

  for (int point=1;point<pointCount;point++) {
    if (((x == points[point-1].x) && (y == points[point-1].y))
               ||((x == points[point].x) && (y == points[point].y))) {
      winding += 2 * pi;
    } else {
      U.Set(points[point-1].x - x,points[point-1].y - y);
      V.Set(points[point].x - x,points[point].y - y);
      winding += VectorAngle(U,V);                          
    }
  }
  if (AbsVal(winding) > G_WINDING_THRESHOLD) return 1;
  return 0;
}





//-- ShowProgress -------------------------------------------------------

/* function ShowProgress is a scummy progress indicator */

void ShowProgress (const char *message, int x, int y, int width, int height){

  double progress;
  
  progress = 100 * ((y+1) * width + (x+1)) / (height * width);
  if (progress>gProgress){
    gProgress = progress; 
    fprintf(stderr, "%s : %.0f%% \r",message,gProgress);
  }
}














