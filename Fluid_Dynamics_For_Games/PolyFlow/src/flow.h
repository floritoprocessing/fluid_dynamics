/*
 * flow.h
 *
 * contains the prototypes for functions used to calculate the flow for polygons
 * 
 *                 
 */

#ifndef _FLOW_H_
#define _FLOW_H_

#include <tcl.h>
#include <tk.h>
#include "globals.h"



/*
 * DoFlowCalculation calculates the flow for the polygons over a series of images
 *
 *
 */

int DoFlowCalculation(ClientData clientData, Tcl_Interp *interp, 
	  int argc, const char *argv[]);
// C_DoFlowCalculation <startFrame> <endFrame>


/*
 * InsidePolygon determines if the point [x,y] is inside the polygon 
 * specified by "points"
 *
 */

int InsidePolygon(int x, int y, int pointCount, Point2D *points);

#endif
