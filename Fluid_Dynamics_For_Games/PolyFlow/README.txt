Polygonal Flow Finder Tool - README file

Author  : Dion Crannitch <dion@atlas.otago.ac.nz>
Date    : 28th July 1999

Version : 1.2
	: Added ability to turn off "backgrounding" for Polygon 0,
	: and fixed some bugs.

INTRODUCTION

The Polygonal Flow Finder Tool is a Tcl/Tk tool for finding ground truth optical flow in real scenes consisting of polygonal objects. Use the tool to fit polygons to the objects in the scene over a sequence of images. You can then output PCM "flow map" files corresponding to the flow between images.

The flow maps are calculated using a perspective transformation technique. For each [x,y] coordinate in the map we determine which polygon the pixel lies in using a winding test. Then we use a perspective transformation from the polygon in the current frame to the same polygon in the next frame to determine where the pixel will move to. Subtracting the two pixels gives a flow vector which is written to the flow map. Pixels which do not lie on an object are background pixels. These pixels are handled by defining a polygon which lies in the background plane and using this as the transform polygon. 


LIMITATIONS

The perspective transformation used is a quadrilateral transformation. Therefore it will only work with four-sided polygons. Polygons with <>4 sides are ignored. 

Occlusion of polygons is not handled. All the points of each polygon must be visible in all frames of the image sequence.

The background of the scene must be a plane. All background pixels are assumed to be in the same plane as the background polygon which is used to determine their flow. See the sample scene provided for an example of what a scene should look like.


USAGE

Execute the script "polyflow" to run the program. The "PolyFlow" window will open and you will be presented with a file browser. Use the file browser to select the sequence of images you wish to load (they must be in GIF format with the extension ".gif"). The images you select will be loaded into the PolyFlow window. Use the "Prev" and "Next" buttons to cycle through them.

To add a polygon, choose "Add Polygon" from the "Polygon" menu. You will be presented with a dialog for selecting the number of points and colors of the polygon. After clicking "OK" a polygon will appear in the PolyFlow window. You can manipulate this polygon with the mouse. The polygon will be added to every image you loaded. You should position the polygon in all of the frames before adding another polygon. Clicking on a point with Button 1 on the mouse will move only that point of the polygon. Button 2 will move the whole polygon.

The first polygon you specify (called "Polygon 0") must be the background polygon. Position its points on four features in the image which lie in the background plane. You can turn this off using the "Use Polygon 0 for background flow" checkbox in the "Show/Hide Polygons" dialog. With this off, Polygon 0 will be treated like an ordinary polygon and any point which is not inside a polygon will be given zero flow.

The polygon points are labelled so you can ensure that the same same point corresponds to the same feature in the image across the whole image sequence. 

You can adjust the colors of polygons by choosing "Change Colors" from the "Polygon" menu. This is useful for making it easier to distinguish between different polygons.

You can show or hide polygon lines, markers, and labels by choosing "Show/Hide Polygons" from the "Polygon" menu. 

Adjacent polygons should have their adjacent points at the same [x,y] coordinate. The "Merge Markers" option in the "Show/Hide Polygons" dialog allows you to move multiple points at the same position at the same time.

You can save the polygons at any time by choosing "Save Polygons" from the "File" menu. Save to a file with a ".pol" extension. To load a polygon file choose "Load Polygons" from the "File" menu.

Once you have positioned all the polygons in the image sequence choose "Calculate Flow" from the "Flow" menu. Use the directory browser to select a directory to write the flow maps to. The flow maps will be written to PCM files with the filename "flow.%03d.pcm" in the directory you specified. The PCM files are numbered such that "flow.001.pcm" gives the flow from frame 0 to frame 1.







