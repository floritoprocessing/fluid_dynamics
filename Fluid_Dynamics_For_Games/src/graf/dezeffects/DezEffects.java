package graf.dezeffects;

import graf.gamefluids.FluidRect;
import processing.core.PApplet;
import processing.core.PImage;

public class DezEffects extends PApplet {
	
	private static int[] createBrightnessArray(PImage image) {
		int[] out = new int[image.pixels.length];
		for (int i=0;i<image.pixels.length;i++) {
			out[i] = 
				(image.pixels[i]>>16&0xff)+
				(image.pixels[i]>>8&0xff)+
				(image.pixels[i]&0xff);
		}
		return out;
	}
	
	private static float[] createDifferenceArray(int[] in1, int[] in0) {
		//diff[index] = new int[len];
		float[] out = new float[in1.length];
		//in: 0..765
		for (int i=0;i<out.length;i++) {
			out[i] = (in1[i]-in0[i])/765.0f;
			//diff[index][i] = bright[index][i]-bright[index-1][i];
		}
		return out;
	}
	private static float[] createMotionGradient(int[] bright, int w, int h, boolean horizontal) {
		//horiz[index] = new float[len];
		//vert[index] = new float[len];
		float[] out = new float[bright.length];
		int pi = 1+w;
		int off = horizontal?1:w;
		for (int y=1;y<h-1;y++) {
			for (int x=1;x<w-1;x++) {
				out[pi] = (bright[pi-off]-bright[pi+off])/765.0f;
				//horiz[index][pi] = (bright[index][pi-1]-bright[index][pi-1])/765.0f; //0..765
				//vert[index][pi] = (bright[index][pi+w]-bright[index][pi-w])/765.0f; //0..765
				pi++;
			}
			pi+=2;
		}
		return out;
	}
	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.dezeffects.DezEffects"});
	}
	/*private final String imgPath = 
		"C:\\Documents and Settings\\mgraf\\My Documents\\_priv" +
		"\\Video Effects Dez\\frames\\";*/
	/*private final String imgPath = 
		"C:\\Documents and Settings\\mgraf\\My Documents\\_priv" +
		"\\Video Effects Dez\\frames50pBW\\";*/
	private final String imgPath = "D:\\_PROJECTS\\Video Effects Dez\\frames50pBW\\";
	
	private final String imgName = "StukjeDez";
	
	private final String imgExt = ".bmp";
	
	private final int imgMin = 180;
	private final int imgMax = 340; //340
	private final int imgJump = 2;
	private final int imgCount = (imgMax-imgMin)/imgJump;
	
	PImage[] img = new PImage[imgCount];
	
	private final int lowResScale = 4;
	private int IMAGE_WIDTH, IMAGE_HEIGHT;
	private int LO_RES_WIDTH, LO_RES_HEIGHT;
	
	
	float DT = 1.0f;
	float DIFF = 0.00001f;
	float VISC = 0.0001f;
	private FluidRect fluid;
	
	boolean smooth = false;
	//boolean median = false;
	
	/**
	 * Brightness array. Values 0..765
	 */
	int[][] bright = new int[imgCount][];
	
	/**
	 * -1..1
	 */
	float[][] diff = new float[imgCount][];
	
	/**
	 * -1..1
	 */
	float[][] horiz = new float[imgCount][];
	
	/**
	 * -1..1
	 */
	float[][] vert = new float[imgCount][];
	
	
	public void keyPressed() {
		if (key=='s') {
			smooth = !smooth;
			println("smooth gradient: "+smooth);
		}
		/*else if (key=='m') {
			median = !median;
			println("median: "+median);
		}*/
	}
	
	
	public void draw() {
		
		background(200);
		
		/*
		 * Get movie image
		 */
		int imageIndex = (frameCount-1)%imgCount;
		if (img[imageIndex]==null) {
			loadAndPrepareArrays(imageIndex);//loadImage(imgPath+imgName+nf(imageIndex+imgMin,4)+imgExt);
		}
		image(img[imageIndex],10,10);
		
		
		
		int len = img[imageIndex].pixels.length;
		//int W = img[imageIndex].width;
		//int H = img[imageIndex].height;
		
		if (imageIndex>0) {
			
			int i1 = imageIndex;
			int i0 = i1-1;
			
			/*
			 * Optional: show difference image
			 */
			if (diff[imageIndex]!=null) {
				PImage diffImg = new PImage(IMAGE_WIDTH,IMAGE_HEIGHT);
				for (int i=0;i<len;i++) {
					int br = (int)(255*(diff[imageIndex][i]*0.5+0.5));
					diffImg.pixels[i] = color(br,br,br);
				}
				image(diffImg,IMAGE_WIDTH+20,10);
			}
			
			/*
			 * Create gradient
			 */
			
			if (diff[imageIndex]!=null) {
				
				float vx[] = new float[len];
				float vy[] = new float[len];
				float threshSq = sq(0.005f);
				
				int index=1+IMAGE_WIDTH;
				for (int y=1;y<IMAGE_HEIGHT-1;y++) {
					for (int x=1;x<IMAGE_WIDTH-1;x++) {
						vx[index] = diff[imageIndex][index] * (horiz[imageIndex][index]-horiz[imageIndex-1][index]);
						vy[index] = diff[imageIndex][index] * (vert[imageIndex][index]-vert[imageIndex-1][index]);
						if (vx[index]*vx[index]+vy[index]*vy[index]<threshSq) {
							vx[index]=0;
							vy[index]=0;
						}
						index++;
					}
					index+=2;
				}
				
				
				
				/*
				 * Smooth the gradient
				 */
				
				if (smooth) Smooth.filter(vx,vy,IMAGE_WIDTH,IMAGE_HEIGHT);
				
				
				/*
				 * Display the gradient
				 */
				
				int xb = 10;
				int yb = IMAGE_HEIGHT+20;
				float dispFac = 2;
				float velScaleFac = 25*dispFac;
				
				
				rectMode(CORNER);
				stroke(0);
				fill(255);
				rect(xb,yb,IMAGE_WIDTH*dispFac,IMAGE_HEIGHT*dispFac);
				
				index=1+IMAGE_WIDTH;
				
				for (int y=1;y<IMAGE_HEIGHT-1;y++) {
					for (int x=1;x<IMAGE_WIDTH-1;x++) {
						if (vx[index]!=0||vy[index]!=0) {
							float x0 = xb + x*dispFac;
							float y0 = yb + y*dispFac;
							float x1 = x0 + velScaleFac*vx[index];
							float y1 = y0 + velScaleFac*vy[index];
							stroke(255,0,0);
							line(x0,y0,x1,y1);
							//stroke(0,0,0,32);
							//point(x0,y0);
							
						}
						index++;
					}
					index+=2;
				}
				
				
				/*
				 * create low res average vector field
				 */
				//int lowResScale = 4;
				//int lowW = W/lowResScale;
				//int lowH = H/lowResScale;
				
				boolean firstCreation = false;
				if (LO_RES_WIDTH==0) firstCreation=true;
				
				LO_RES_WIDTH = IMAGE_WIDTH/lowResScale;
				LO_RES_HEIGHT = IMAGE_HEIGHT/lowResScale;
				
				int lowLen = LO_RES_WIDTH*LO_RES_HEIGHT;
				//println(lowW+"/"+lowH);
				
				float[] vxL = new float[lowLen];
				float[] vyL = new float[lowLen];
				
				index=1+LO_RES_WIDTH;
				for (int y=1;y<LO_RES_HEIGHT-1;y++) {
					for (int x=1;x<LO_RES_WIDTH-1;x++) {
						for (int sy=0;sy<lowResScale;sy++) for (int sx=0;sx<lowResScale;sx++) {
							int i = (sx+x*lowResScale) + IMAGE_WIDTH*(sy+y*lowResScale);
							vxL[index] += vx[i];
							vyL[index] += vy[i];
						}
						index++;
					}
					index+=2;
				}
				
				/*
				 * Draw low res average vector field
				 */
				
				//int xb = 10;
				yb += IMAGE_HEIGHT*dispFac+10;//H+20;
				velScaleFac = 10;
				
				fill(255);
				stroke(0);
				rect(xb,yb,LO_RES_WIDTH*lowResScale,LO_RES_HEIGHT*lowResScale);
				index=1+LO_RES_WIDTH;
				for (int y=1;y<LO_RES_HEIGHT-1;y++) {
					for (int x=1;x<LO_RES_WIDTH-1;x++) {
						if (vxL[index]!=0||vyL[index]!=0) {
							float x0 = xb + x*lowResScale;
							float y0 = yb + y*lowResScale;
							float x1 = x0 + velScaleFac*vxL[index];
							float y1 = y0 + velScaleFac*vyL[index];
							stroke(255,0,0);
							line(x0,y0,x1,y1);
						}
						index++;
					}
					index+=2;
				}
				
				
				
				/*
				 * Create fluid;
				 */
				
				if (firstCreation) {
					// create flowField
					fluid = new FluidRect(LO_RES_WIDTH,LO_RES_HEIGHT);
					println("fluid created");
				}
				
				/*
				 * Add to fluid
				 */
				
				float[] newDens = new float[fluid.size];
				float[] newU = new float[fluid.size];
				float[] newV = new float[fluid.size];
				int i=0;
				for (int y=0;y<LO_RES_HEIGHT;y++) {
					for (int x=0;x<LO_RES_WIDTH;x++) {
						if (vxL[i]!=0||vyL[i]!=0) {
							
							newDens[i] = min(0.1f, dist(0, 0, vxL[i], vyL[i]));//0.1f;
							newU[i] = vxL[i];
							newV[i] = vyL[i];
						}
						i++;
					}
				}
				fluid.step(newDens, newU, newV, DIFF, VISC, DT);
				
				/*
				 * Draw fluid
				 */
				
				xb += IMAGE_WIDTH+10;
				stroke(0);
				fill(255);
				rect(xb,yb,IMAGE_WIDTH,IMAGE_HEIGHT);
				
				rectMode(CORNER);
				//float[] dens = fluid.getDensity();
				i=0;
				for (int y=0;y<fluid.HEIGHT;y++) {
					for (int x=0;x<fluid.WIDTH;x++) {
						//int col = color(max(255,dens[i++]*255),0,0);
						int col = color(255*fluid.getDensity(x, y));
						fill(col);
						rect(xb+x*lowResScale,yb+y*lowResScale,lowResScale,lowResScale);
					}
				}
				
				fluid.evaporate(0.01f, DT);
				
				
			}
			
		}
		
	}

	private void load(int count) {
		for (int i=0;i<count;i++) {
			loadAndPrepareArrays(i);
			if (i%10==9) println("loaded "+(i+1)+" of "+img.length);
		}
	}

	/**
	 * Load image
	 * create bright array
	 * create diff array
	 * create horiz and vert array
	 * @param index
	 */
	private void loadAndPrepareArrays(int index) {
		//boolean median = true;
		
		
		/*
		 * Load image
		 */
		img[index] = loadImage(imgPath+imgName+nf(index*imgJump+imgMin,4)+imgExt);
		if (IMAGE_WIDTH==0) {
			IMAGE_WIDTH = img[index].width;
			IMAGE_HEIGHT = img[index].height;
		}
		Median.filter(img[index]);
		
		int len = img[index].width*img[index].height;
		int w = img[index].width;
		int h = img[index].height;
		
		
		// 0..765
		// create brightness image
		bright[index] = createBrightnessArray(img[index]);
		
		
		// create horiz/vert gradient
		horiz[index] = createMotionGradient(bright[index],w,h,true);
		vert[index] = createMotionGradient(bright[index],w,h,false);
		
		if (index>0) {
			
			diff[index] = createDifferenceArray(bright[index],bright[index-1]);
			
			
		}

		
		
		
		
	}

	public void setup() {
		size(550,800,P3D);
		//convertImages(imgPath, imgPath2, 0.5f, true);
		//System.exit(0);
		load(40);
		frameRate(25);
	}
	
	
	
	
	/*private void convertImages(String srcPath, String dstPath, float scaleFac, boolean blackAndWhite) {
	File[] lst = new File(srcPath).listFiles();
	PImage src;
	
	for (int i=0;i<lst.length;i++) {
		println("converting "+lst[i].getName());
		src = loadImage(lst[i].getAbsolutePath());
		int w = (int)(src.width*scaleFac);
		int h = (int)(src.height*scaleFac);
		PGraphics dst = createGraphics(w, h, P3D);
		dst.beginDraw();
		dst.background(0);
		dst.image(src,0,0,w,h);
		dst.filter(GRAY);
		dst.endDraw();
		
		PImage out = new PImage(w,h);
		out.pixels = dst.pixels;
		out.updatePixels();
		
		String name = dstPath+lst[i].getName();
		(new File(name)).mkdirs();
		println("saving to "+name);
		out.save(name);
		
		
	}
}*/
	
}
