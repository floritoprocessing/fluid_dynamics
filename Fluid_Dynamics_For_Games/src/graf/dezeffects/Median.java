package graf.dezeffects;

import java.util.Arrays;

import processing.core.PImage;

public class Median {
	
	public static int SIZE = 1;
	
	public static void filter(PImage in) {
		
		
		in.loadPixels();
		int width = in.width;
		int height = in.height;
		
		/*
		 * Calc brightness
		 */
		int[] bright = new int[in.pixels.length];
		for (int i=0;i<in.pixels.length;i++) {
			bright[i] = ((in.pixels[i]>>16&0xff) + (in.pixels[i]>>8&0xff) + (in.pixels[i]&0xff))/3; 
		}
		
		PImage out = new PImage(width,height);
		out.format = in.format;
		
		int index = 1+width;
		
		if (SIZE==1) {
			int[] br = new int[9];
			for (int y=1;y<height-1;y++) {
				for (int x=1;x<width-1;x++) {
					br[0] = bright[index-width-1];
					br[1] = bright[index-width];
					br[2] = bright[index-width+1];
					br[3] = bright[index-1];
					br[4] = bright[index];
					br[5] = bright[index+1];
					br[6] = bright[index+width-1];
					br[7] = bright[index+width];
					br[8] = bright[index+width+1];
					Arrays.sort(br);
					out.pixels[index] = 0xff<<24 | br[4]<<16 | br[4]<<8 | br[4];
					index++;
				}
				index+=2;
			}
			int m = (height-1)*width;
			for (int x=0;x<width;x++) {
				out.pixels[x] = in.pixels[x];
				out.pixels[m-x] = in.pixels[m-x];
			}
			for (int y=width;y<m;y+=width) {
				out.pixels[y] = in.pixels[y];
				out.pixels[y+width-1] = in.pixels[y+width-1];
			}
		}
		
		
		
		
		out.updatePixels();
		
		System.arraycopy(out.pixels, 0, in.pixels, 0, in.pixels.length);
		//in = out;
		
		//return out;
	}

}
