package graf.dezeffects;

public class Smooth {

	private static double kernel[][] = {
		{0.01,0.01,0.01},
		{0.01,0.92,0.01},
		{0.01,0.01,0.01}
	};
	
	public static void filter(float[] ix, float[] iy, int width, int height) {
		float[] ox = new float[ix.length];
		float[] oy = new float[iy.length];
		
		int srcI, srcX, srcY, dstI=0;
		int xo, yo, x, y;
		for (y=0;y<height;y++) {
			for (x=0;x<width;x++) {
			
				for (yo=-1;yo<=1;yo++) {
					srcY = y+yo;
					for (xo=-1;xo<=1;xo++) {
						srcI = dstI + xo + yo*width;
						srcX = x+xo;
						if (srcX>=0&&srcX<width&&srcY>=0&&srcY<height) {
							ox[dstI] += kernel[xo+1][yo+1]*ix[srcI];
							oy[dstI] += kernel[xo+1][yo+1]*iy[srcI];
						}
					}
				}
				dstI++;
				
				
			}
		}
		
		System.arraycopy(ox, 0, ix, 0, ix.length);
		System.arraycopy(oy, 0, iy, 0, iy.length);
		
		//ix=null;
	}
	
	
	
	

}
