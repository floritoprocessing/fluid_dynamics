package graf.gradient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ColorGradient {

	public static final ColorGradient FIRE; 
	static {
		FIRE = new ColorGradient(
				new int[] {		0xff000000,	0xff261f1e, 0xff841001,	0xffed6c0e,	0xfff9febb},
				new float[] {	0,			0.18f,		0.7f,		0.85f,		1} //0.5 0.75 0.98
				);
	}
	
	public static final ColorGradient FIRE_TRANSPARENT; 
	static {
		FIRE_TRANSPARENT = new ColorGradient(
				new int[] {		0x01000000,	0x30261f1e, 0x55841001,	0x77ed6c0e,	0x88f9febb},
				new float[] {	0,			0.18f,		0.7f,		0.85f,		1} //0.5 0.75 0.98
				);
	}
	
	
	public static final ColorGradient random(int nrOfSteps, boolean sortBright) {
		RGB[] col = new RGB[nrOfSteps];
		float[] pos = new float[nrOfSteps];
		for (int i=0;i<col.length;i++) {
			col[i] = new RGB(0xff000000 | (int)(Math.random()*0xffffff));
			pos[i] = (float)Math.random();
		}
		if (sortBright) {
			Arrays.sort(pos);
			Arrays.sort(col);
		}
		int[] c = new int[col.length];
		for (int i=0;i<c.length;i++) {
			c[i] = col[i].col();
		}
		return new ColorGradient(c, pos);
	}
	
	ArrayList<Col> colorPoints = new ArrayList<Col>();
	float minPos=0, maxPos=1;
	
	private static class RGB implements Comparable {
		int r, g, b;
		RGB(int color) {
			r = color>>16&0xff;
			g = color>>8&0xff;
			b = color&0xff;
		}
		int bright() {
			return r+g+b;
		}
		int col() {
			return 0xff<<24|r<<16|g<<8|b;
		}
		public int compareTo(Object arg0) {
			RGB other = (RGB)arg0;
			if (bright()<other.bright()) return -1;
			else if (bright()>other.bright()) return 1;
			else return 0;
		}
	}
	
	private class Col implements Comparable {
		int color;
		float position;
		Col(int color, float position) {
			this.color = color;
			this.position = position;
		}
		public int compareTo(Object arg0) {
			Col other = (Col)arg0;
			if (position<other.position) return -1;
			else if (position>other.position) return 1;
			else return 0;
		}
		public String toString() {
			return position+", color="+color;
		}
	}
	
	public ColorGradient(int color) {
		colorPoints.add(new Col(color,0));
		minPos=0;
		maxPos=0;
	}
	
	public ColorGradient(int[] colors, float[] positions) {
		if (colors.length!=positions.length) {
			throw new RuntimeException("colors array not the same length as positions array!");
		} else {
			for (int i=0;i<colors.length;i++) {
				colorPoints.add(new Col(colors[i],positions[i]));
			}
			Collections.sort(colorPoints);
		}
		minPos = colorPoints.get(0).position;
		maxPos = colorPoints.get(colorPoints.size()-1).position;
		/*for (int i=0;i<colorPoints.size();i++)
			System.out.println(colorPoints.get(i));*/
	}
	
	public int getColorAt(float pos) {
		if (pos<minPos) {
			return colorPoints.get(0).color;
		} else if (pos>maxPos) {
			return colorPoints.get(colorPoints.size()-1).color;
		} else {
			int index=0;
			float p0=0, p1=0;
			boolean found = false;
			while (!found) {
				p0 = colorPoints.get(index).position;
				p1 = colorPoints.get(index+1).position;
				if (pos>=p0&&pos<p1) {
					found = true;
				} else {
					index++;
					if (index==colorPoints.size()-2) found=true;
				}
			}
			return interpolateColor(pos,p0,p1,colorPoints.get(index).color,colorPoints.get(index+1).color);
		}
	}

	private int interpolateColor(float in, float in0, float in1, int c0, int c1) {
		float p = (in-in0)/(in1-in0);
		p = Math.max(0,Math.min(1,p));
		//if (p<0||p>1) throw new RuntimeException("something is wrong");
		int a0 = c0>>24&0xff, a1 = c1>>24&0xff;
		int r0 = c0>>16&0xff, r1 = c1>>16&0xff;
		int g0 = c0>>8&0xff, g1 = c1>>8&0xff;
		int b0 = c0&0xff, b1 = c1&0xff;
		int a = (int)(a0+p*(a1-a0));
		int r = (int)(r0+p*(r1-r0));
		int g = (int)(g0+p*(g1-g0));
		int b = (int)(b0+p*(b1-b0));
		return a<<24|r<<16|g<<8|b;
	}
	
}
