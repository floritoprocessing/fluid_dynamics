package graf.gamefluids;

public class FluidBox {

	/**
	 * Amount of boxes in fist dimension excluding boundary boxes
	 */
	private final int X;

	/**
	 * Amount of boxes in first dimension including boundary boxes (X+2)
	 */
	public final int WIDTH;

	/**
	 * Amount of boxes in second dimension excluding boundary boxes
	 */
	private final int Y;

	/**
	 * Amount of boxes in second dimension including boundary boxes (X+2)
	 */
	public final int HEIGHT;
	private final int WH;
	
	/**
	 * Amount of boxes in third dimension excluding boundary boxes
	 */
	private final int Z;
	
	/**
	 * Amount of boxes in second dimension including boundary boxes (X+2)
	 */
	public final int DEPTH;
	//private static float SKIP_THRESH = 0.001f;
	
	/**
	 * Size of the entire array
	 */
	public final int size;

	private float[] u, v, w;
	private float[] u_prev, v_prev, w_prev;

	private float[] dens;
	private float[] dens_prev;

	public FluidBox(int WIDTH, int HEIGHT, int DEPTH) {
		this.WIDTH = WIDTH;
		this.HEIGHT = HEIGHT;
		this.DEPTH = DEPTH;
		WH = WIDTH*HEIGHT;
		X = WIDTH - 2;
		Y = HEIGHT - 2;
		Z = DEPTH - 2;
		size = WIDTH * HEIGHT * DEPTH;
		u = new float[size];
		v = new float[size];
		w = new float[size];
		u_prev = new float[size];
		v_prev = new float[size];
		w_prev = new float[size];
		dens = new float[size];
		dens_prev = new float[size];
	}

	private void add_source(float[] x, float[] s, float dt) {
		int i;//, size = (N + 2) * (N + 2);
		for (i = 0; i < size; i++)
			x[i] += dt * s[i];
	}
	
	private void advect(int b, float[] d, float[] d0, float[] u, float[] v, float[] w, float dt) {
		int i, j, k, i0, j0, k0, i1, j1, k1;
		float x, y, z, s0, t0, u0, s1, t1, u1, dt0;
		//dt0 = dt * N;
		dt0 = dt * (float)Math.sqrt(X*Y*Z);//(X+Y)/2;
		// or sqrt(X*X+Y*Y)
		int srcIndex = 1+WIDTH+WH;
		for (k = 1; k <= Z; k++) {
			for (j = 1; j <= Y; j++) {
				for (i = 1; i <= X; i++) {
					//x = i - dt0 * u[IX(i, j)];
					//y = j - dt0 * v[IX(i, j)];
					x = i - dt0 * u[srcIndex];
					y = j - dt0 * v[srcIndex];
					z = k - dt0 * w[srcIndex];
					
					if (x < 0.5) x = 0.5f;
					if (x > X + 0.5) x = X + 0.5f;
					i0 = (int) x;
					i1 = i0 + 1;
					
					if (y < 0.5) y = 0.5f;
					if (y > Y + 0.5) y = Y + 0.5f;
					j0 = (int) y;
					j1 = j0 + 1;
					
					if (z < 0.5) z = 0.5f;
					if (z > Z + 0.5) z = Z + 0.5f;
					k0 = (int) z;
					k1 = k0 + 1;
					
					s1 = x - i0;
					s0 = 1 - s1;
					t1 = y - j0;
					t0 = 1 - t1;
					u1 = z - k0;
					u0 = 1 - u1;
					
					//float ySum0 = t0 * d0[IX(i0, j0, k0)] + t1 * d0[IX(i0, j1, k0)];
					//float ySum1 = t0 * d0[IX(i1, j0, k0)] + t1 * d0[IX(i1, j1, k0)];
					
					d[srcIndex] = 
						//s0 * (t0 * d0[IX(i0, j0, k0)] + t1 * d0[IX(i0, j1, k0)]) +
						//s1 * (t0 * d0[IX(i1, j0, k0)] + t1 * d0[IX(i1, j1, k0)]);
					
						s0 * (
							t0 * (u0 * d0[coordsToIndex(i0, j0, k0)] + u1 * d0[coordsToIndex(i0, j0, k1)]) +
							t1 * (u0 * d0[coordsToIndex(i0, j1, k0)] + u1 * d0[coordsToIndex(i0, j1, k1)])
						) + s1 * (
							t0 * (u0 * d0[coordsToIndex(i1, j0, k0)] + u1 * d0[coordsToIndex(i1, j0, k1)]) +
							t1 * (u0 * d0[coordsToIndex(i1, j1, k0)] + u1 * d0[coordsToIndex(i1, j1, k1)])
						)
						;
						
					
					srcIndex++;
				}
				srcIndex+=2;
			}
			srcIndex += 2*WIDTH;
		}
		set_bnd3d(b, d);
	}

	public int coordsToIndex(int x, int y, int z) {
		return x + WIDTH * y + WH*z;
	}

	private void dens_step(
			float[] x, float[] x0, 
			float[] u, float[] v, float[] w,
			float diff,	float dt) {
		add_source(x, x0, dt);
		
		// SWAP ( x0, x );
		float[] tmp = x0;
		x0 = x;
		x = tmp;
		diffuse(0, x, x0, diff, dt);
		
		// SWAP ( x0, x );
		tmp = x0;
		x0 = x;
		x = tmp;
		
		advect(0, x, x0, u, v, w, dt);
		
	}
	
	private void diffuse(int b, float[] x, float[] x0, float diff, float dt) {
		int i, j, k, it;
		float a = dt * diff * X * Y;
		float divider = 1/(1+6*a); //1/(1+4*a);
		
		for (it = 0; it < 20; it++) {
			int srcIndex = 1*WH+1*WIDTH+1;
			
			for (k = 1; k <= Z; k++) {
				for (j = 1; j <= Y; j++) {
					for (i = 1; i <= X; i++) {										
						x[srcIndex] = (x0[srcIndex] + a * (
								x[srcIndex-1] + 
								x[srcIndex+1] + 
								x[srcIndex-WIDTH] + 
								x[srcIndex+WIDTH] +
								x[srcIndex-WH] +
								x[srcIndex+WH])) * divider;
						
						srcIndex++;
					}
					srcIndex+=2;
				}
				srcIndex += 2*WIDTH;
			}
			
			set_bnd3d(b, x);
		}
	}
	
	public void evaporate(float f, float dt) {
		for (int i=0;i<dens.length;i++) {
			dens[i] -= f*dt;
			if (dens[i]<0) dens[i]=0;
		}
	}

	public float[] getDensity() {
		return dens;
	}
	
	public float getDensity(int x, int y, int z) {
		return dens[coordsToIndex(x, y, z)];
	}

	public float[] getVelocityX() {
		return u;
	}

	public float getVelocityX(int x, int y, int z) {
		return u[coordsToIndex(x, y, z)];
	}

	public float[] getVelocityY() {
		return v;
	}

	public float getVelocityY(int x, int y, int z) {
		return v[coordsToIndex(x, y, z)];
	}
	
	
	public float[] getVelocityZ() {
		return w;
	}

	public float getVelocityZ(int x, int y, int z) {
		return w[coordsToIndex(x, y, z)];
	}

	
	
	private void project(float[] u, float[] v, float[] w, float[] p, float[] div) {
		int i, j, k, it;
		float h;
		h = 1.0f / Math.max(X,Math.max(Y,Z));//(float)Math.sqrt(X*Y);
		int srcIndex = 1+WIDTH+WH;
		
		for (k = 1; k <= Z; k++) {
			for (j = 1; j <= Y; j++) {
				for (i = 1; i <= X; i++) {
					//div[IX(i, j)] = -0.5f
					div[srcIndex] = -0.5f
							* h
							* (
									u[srcIndex+1] - 
									u[srcIndex-1] + 
									v[srcIndex+WIDTH] - 
									v[srcIndex-WIDTH] +
									w[srcIndex+WH] - 
									w[srcIndex-WH]);
					p[srcIndex] = 0;
					srcIndex++;
				}
				srcIndex+=2;
			}
			srcIndex += 2*WIDTH;
		}
		
		set_bnd3d(0, div);
		set_bnd3d(0, p);
		
		
		for (it = 0; it < 20; it++) {
			srcIndex = 1+WIDTH+WH;
			for (k = 1; k <= Z; k++) {
				for (j = 1; j <= Y; j++) {
					for (i = 1; i <= X; i++) {
						p[srcIndex] = (div[srcIndex] + 
								p[srcIndex-1] + p[srcIndex+1] 
								+ p[srcIndex-WIDTH] + p[srcIndex+WIDTH]
								+ p[srcIndex-WH] + p[srcIndex+WH]) / 6;
						srcIndex++;
					}
					srcIndex+=2;
				}
				srcIndex += 2*WIDTH;
			}
			set_bnd3d(0, p);
		}
		
		
		srcIndex = 1+WIDTH+WH;
		float lstp = 0.5f/h;
		for (k = 1; k <= Z; k++) {
			for (j = 1; j <= Y; j++) {
				for (i = 1; i <= X; i++) {
					u[srcIndex] -= lstp * (p[srcIndex+1] - p[srcIndex-1]);
					v[srcIndex] -= lstp * (p[srcIndex+WIDTH] - p[srcIndex-WIDTH]);
					w[srcIndex] -= lstp * (p[srcIndex+WH] - p[srcIndex-WH]);
					srcIndex++;
				}
				srcIndex+=2;
			}
			srcIndex += 2*WIDTH;
		}
		set_bnd3d(1, u);
		set_bnd3d(2, v);
		set_bnd3d(3, w);
	}

		
	private void set_bnd3d(int b, float[] x) {
		int i, j;
		/*
		 * Left and Right wall
		 */
		int srcIndex0 = 1*WH + WIDTH; //p(0,1,1)
		int srcIndex1 = 1*WH + WIDTH+X+1; //p(X+1,1,1)
		for (i = 1; i <= Z; i++) {
			for (j = 1; j <= Y; j++) {
				x[srcIndex0] = b == 1 ? -x[srcIndex0+1] : x[srcIndex0+1];
				x[srcIndex1] = b == 1 ? -x[srcIndex1-1] : x[srcIndex1-1];
				srcIndex0 += WIDTH;
				srcIndex1 += WIDTH;
			}
			srcIndex0 += 2*WIDTH;
			srcIndex1 += 2*WIDTH;
		}
		/*
		 * Top and bottom wall
		 */
		srcIndex0 = 1 + (0*WIDTH) + (1*WH); //p(1,0,1)
		srcIndex1 = 1 + (Y+1)*WIDTH + (1*WH); //p(1,Y+1,1)
		for (i = 1; i <= Z; i++) {
			for (j = 1; j <= X; j++) {	
				x[srcIndex0] = b == 2 ? -x[srcIndex0+WIDTH] : x[srcIndex0+WIDTH];
				x[srcIndex1] = b == 2 ? -x[srcIndex1-WIDTH] : x[srcIndex1-WIDTH];
				srcIndex0++;
				srcIndex1++;
			}
			srcIndex0 += (2-WIDTH) + WH;
			srcIndex1 += (2-WIDTH) + WH;
		}
		/*
		 * Front and back wall
		 */
		srcIndex0 = 1 + (1*WIDTH) + (0*WH); //p(1,1,0)
		srcIndex1 = 1 + (1*WIDTH) + ((Z+1)*WH); //p(1,1,Z+1);
		for (i = 1; i <= Y; i++) {
			for (j = 1; j <= X; j++) {
				x[srcIndex0] = b == 3 ? -x[srcIndex0+WH] : x[srcIndex0+WH];
				x[srcIndex1] = b == 3 ? -x[srcIndex1-WH] : x[srcIndex1-WH];
				srcIndex0++;
				srcIndex1++;
			}
			srcIndex0+=2;
			srcIndex1+=2;
		}
		
		/*
		 * Top edges
		 */
		srcIndex0 = 1; // p(1,0,0); front
		srcIndex1 = 1 + (Z+1)*WH; // p(1,0,Z+1); back
		for (i = 1; i < X; i++) {
			x[srcIndex0] = 0.5f * (x[srcIndex0+WIDTH] + x[srcIndex0+WH]);
			x[srcIndex1] = 0.5f * (x[srcIndex1+WIDTH] + x[srcIndex1-WH]);
			srcIndex0++;
			srcIndex1++;
		}
		/*
		 * Bottom edges
		 */
		srcIndex0 = 1 + (Y+1)*WIDTH; // p(1,Y+1,0); front
		srcIndex1 = 1 + (Y+1)*WIDTH + (Z+1)*WH; // p(1,Y+1,Z+1); back
		for (i = 1; i < X; i++) {
			x[srcIndex0] = 0.5f * (x[srcIndex0-WIDTH] + x[srcIndex0+WH]);
			x[srcIndex1] = 0.5f * (x[srcIndex1-WIDTH] + x[srcIndex1-WH]);
			srcIndex0++;
			srcIndex1++;
		}
		/*
		 * Left edges
		 */
		srcIndex0 = 1*WIDTH; // p(0,1,0) front
		srcIndex1 = 1*WIDTH + (Z+1)*WH; // p(0,1,(Z+1)) back
		for (i = 1; i < Y; i++) {
			x[srcIndex0] = 0.5f * (x[srcIndex0+1] + x[srcIndex0+WH]);
			x[srcIndex1] = 0.5f * (x[srcIndex1+1] + x[srcIndex1-WH]);
			srcIndex0 += WIDTH;
			srcIndex1 += WIDTH;
		}
		/*
		 * Right edges
		 */
		srcIndex0 = (X+1) + 1*WIDTH; // p((X+1),1,0) front
		srcIndex1 = (X+1) + 1*WIDTH + (Z+1)*WH; // p((X+1),1,(Z+1)) back
		for (i = 1; i < Y; i++) {
			x[srcIndex0] = 0.5f * (x[srcIndex0-1] + x[srcIndex0+WH]);
			x[srcIndex1] = 0.5f * (x[srcIndex1-1] + x[srcIndex1-WH]);
			srcIndex0 += WIDTH;
			srcIndex1 += WIDTH;
		}
		
		/*
		 * Corners
		 */
		
		float drie = 1.0f/3;
		//top left front:
		x[coordsToIndex(0,0,0)] = drie * (x[coordsToIndex(1,0,0)] + x[coordsToIndex(0,1,0)] + x[coordsToIndex(0,0,1)]);
		//top right front:
		x[coordsToIndex(X+1,0,0)] = drie * (x[coordsToIndex(X+1-1,0,0)] + x[coordsToIndex(X+1,1,0)] + x[coordsToIndex(X+1,0,1)]);
		//bottom left front:
		x[coordsToIndex(0,Y+1,0)] = drie * (x[coordsToIndex(1,Y+1,0)] + x[coordsToIndex(0,Y+1-1,0)] + x[coordsToIndex(0,Y+1,1)]);
		//bottom right front:
		x[coordsToIndex(X+1,Y+1,0)] = drie * (x[coordsToIndex(X+1-1,Y+1,0)] + x[coordsToIndex(X+1,Y+1-1,0)] + x[coordsToIndex(X+1,Y+1,1)]);
		
		//top left back:
		x[coordsToIndex(0,0,Z+1)] = drie * (x[coordsToIndex(1,0,Z+1)] + x[coordsToIndex(0,1,Z+1)] + x[coordsToIndex(0,0,Z+1-1)]);
		//top right back:
		x[coordsToIndex(X+1,0,Z+1)] = drie * (x[coordsToIndex(X+1-1,0,Z+1)] + x[coordsToIndex(X+1,1,Z+1)] + x[coordsToIndex(X+1,0,Z+1-1)]);
		//bottom left back:
		x[coordsToIndex(0,Y+1,Z+1)] = drie * (x[coordsToIndex(1,Y+1,Z+1)] + x[coordsToIndex(0,Y+1-1,Z+1)] + x[coordsToIndex(0,Y+1,Z+1-1)]);
		//bottom right back:
		x[coordsToIndex(X+1,Y+1,Z+1)] = drie * (x[coordsToIndex(X+1-1,Y+1,Z+1)] + x[coordsToIndex(X+1,Y+1-1,Z+1)] + x[coordsToIndex(X+1,Y+1,Z+1-1)]);
		
		/*x[IX(0, 0)] = 0.5f * (x[IX(1, 0)] + x[IX(0, 1)]);
		x[IX(0, Y + 1)] = 0.5f * (x[IX(1, Y + 1)] + x[IX(0, Y)]);
		x[IX(X + 1, 0)] = 0.5f * (x[IX(X, 0)] + x[IX(X + 1, 1)]);
		x[IX(X + 1, Y + 1)] = 0.5f * (x[IX(X, Y + 1)] + x[IX(X + 1, Y)]);*/
	}

	public void step(
			float[] densPrev, float[] uPrev, float[] vPrev, float[] wPrev,
			float diff, float visc, float dt) {
		
		/*
		 * Diffuse stepper
		 */
		dens_prev = densPrev;
		u_prev = uPrev;
		v_prev = vPrev;
		w_prev = wPrev;
		vel_step(u, v, w, u_prev, v_prev, w_prev, visc, dt);
		dens_step(dens, dens_prev, u, v, w, diff, dt);
	}

	private void vel_step(
			float[] u, float[] v, float[] w,  
			float[] u0, float[] v0, float[] w0, 
			float visc, float dt) {
		
		add_source(u, u0, dt);
		add_source(v, v0, dt);
		add_source(w, w0, dt);

		float[] tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		diffuse(1, u, u0, visc, dt);
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		diffuse(2, v, v0, visc, dt);
		
		tmp = w0;
		w0 = w;
		w = tmp; // SWAP ( w0, w );
		diffuse(2, w, w0, visc, dt);
		
		
		project(u, v, w, u0, v0);
		
		
		tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		
		tmp = w0;
		w0 = w;
		w = tmp; // SWAP ( w0, w );
		
		advect(1, u, u0, u0, v0, w0, dt);
		advect(2, v, v0, u0, v0, w0, dt);
		advect(3, w, w0, u0, v0, w0, dt);
		
		
		project(u, v, w, u0, v0);
		
	}

}
