package graf.gamefluidsdev;

import graf.gamefluids.FluidBox;
import graf.gradient.ColorGradient;
import processing.core.PApplet;
import processing.core.PGraphics;

public class FluidBoxDemo extends PApplet {

	private static final long serialVersionUID = -6209751846308820317L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.gamefluidsdev.FluidBoxDemo"});
	}
	
	ColorGradient gradient;// = ColorGradient.random(20);//FIRE;
	
	FluidBox fluid;
	
	int DRAW_WIDTH_MOUSE_DOWN = 3;
	int DRAW_WIDTH_MOUSE_UP = 5;
	float DRAW_STRENGTH = 0.8f;
	
	float DT = 1.0f;
	float DIFF = 0.00001f;
	float VISC = 0.0001f;
	float EVAPORATE_NORM =  0.0001f;
	float EVAPORATE_CLEAR = 0.01f;
	
	float camX, camY, camZ;
	float worldScale = 8;
	
	PGraphics[] eye = new PGraphics[2];
	int EYE_WIDTH = 160;
	int EYE_SEP = 150;
	
	public void setup() {
		size(600,450,P3D);
		eye[0] = createGraphics(EYE_WIDTH, height, P3D);
		eye[1] = createGraphics(EYE_WIDTH, height, P3D);
		fluid = new FluidBox(27,40,27);
		camX = (fluid.WIDTH/2.0f)*worldScale + 40;
		camY = (fluid.HEIGHT/2.0f)*worldScale - 100;
		camZ = 800;
		background(0);
		loadPixels();
		//gradient = ColorGradient.random(7,true);//FIRE;
		gradient = ColorGradient.FIRE_TRANSPARENT;
	}
	
	public void keyPressed() {
		if (key=='s') {
			save("output/FluidBoxDemo_preview.jpg");
		} else
			setup();
	}
	
	public void mouseDragged() {
		camX += pmouseX-mouseX;
		camY += pmouseY-mouseY;
	}
	
	public void draw() {
		float[] newDens = new float[fluid.size];
		float[] newU = new float[fluid.size];
		float[] newV = new float[fluid.size];
		float[] newW = new float[fluid.size];
		
		int midX = fluid.WIDTH/2;
		int midZ = fluid.DEPTH/2;
		for (int x=midX-2;x<=midX+2;x++) for (int z=midZ-2;z<=midZ+2;z++) {
		int pIndex = fluid.coordsToIndex(x, fluid.HEIGHT-2, z);
		newDens[pIndex] = 0.8f;
		newU[pIndex] = random(-0.001f,0.001f);
		newV[pIndex] = -random(0.5f,1.0f);
		newW[pIndex] = random(-0.001f,0.001f);
		}
		
		fluid.step(newDens, newU, newV, newW, DIFF, VISC, DT);
		fluid.evaporate(EVAPORATE_NORM, DT);
		
		background(32);
		//noStroke();//stroke(255);
		//noFill();

		int col[] = new int[fluid.size];
		int ii=0;
		for (int z=1;z<fluid.DEPTH-1;z++)
			for (int y=1;y<fluid.HEIGHT-1;y++)
				for (int x=1;x<fluid.WIDTH-1;x++)
					col[ii++]=gradient.getColorAt(fluid.getDensity(x,y,z));
		
		
		for (int ei=0;ei<2;ei++) {
			eye[ei].beginDraw();
			eye[ei].background(32);
			eye[ei].camera(
					camX + (ei==0?-5:5), camY , camZ,
					(fluid.WIDTH/2.0f)*worldScale,(fluid.HEIGHT/2.0f)*worldScale,0,
					0,1,0);
			
			eye[ei].scale(worldScale);
			
			eye[ei].pushMatrix();
			eye[ei].translate(fluid.WIDTH*0.5f,fluid.HEIGHT*0.5f,fluid.DEPTH*0.5f);
			eye[ei].stroke(255,255,255,32);
			eye[ei].noFill();
			eye[ei].box(fluid.WIDTH-2,fluid.HEIGHT-2,fluid.DEPTH-2);
			eye[ei].popMatrix();
			
			eye[ei].noStroke();
			ii=0;
			for (int z=1;z<fluid.DEPTH-1;z++) {
				for (int y=1;y<fluid.HEIGHT-1;y++) {
					for (int x=1;x<fluid.WIDTH-1;x++) {
						//float dens = fluid.getDensity(x,y,z);
						int a = col[ii]>>24&0xff;
						if (a>0) {
							eye[ei].pushMatrix();
							eye[ei].translate(x,y,z);
							//	eye[ei].fill(gradient.getColorAt(dens));
							eye[ei].fill(col[ii]);
							eye[ei].box(1);
							eye[ei].popMatrix();
						}
						ii++;
					}
				}
			}
			eye[ei].endDraw();
		}
		
		image(eye[0],(width-EYE_WIDTH)/2-EYE_SEP/2,0);
		image(eye[1],(width-EYE_WIDTH)/2+EYE_SEP/2,0);
		//saveFrame("./outputBox/FluidBoxDemo_####.bmp");
		
		if (keyPressed && key=='S') {
			System.exit(0);
		}
	}

}
