package graf.gamefluidsdev;

public class FluidSquare {

	/**
	 * Amount of boxes in 1 dimension excluding boundary boxes
	 */
	private final int N;

	/**
	 * Amount of boxes in 1 dimension including boundary boxes (N+2)
	 */
	public final int N2;

	/**
	 * Size of the entire array
	 */
	public final int size;

	private float[] u, v;
	private float[] u_prev, v_prev;

	private float[] dens;
	private float[] dens_prev;

	public FluidSquare(int N2) {
		this.N2 = N2;
		N = N2 - 2;
		size = (N2) * (N2);
		u = new float[size];
		v = new float[size];
		u_prev = new float[size];
		v_prev = new float[size];
		dens = new float[size];
		dens_prev = new float[size];
	}

	public int IX(int x, int y) {
		return x + N2 * y;
	}

	public float getDensity(int x, int y) {
		return dens[IX(x, y)];
	}

	public float getVelocityX(int x, int y) {
		return u[IX(x, y)];
	}

	public float getVelocityY(int x, int y) {
		return v[IX(x, y)];
	}

	void dens_step(float[] x, float[] x0, float[] u, float[] v, float diff,
			float dt) {
		add_source(x, x0, dt);
		
		// SWAP ( x0, x );
		float[] tmp = x0;
		x0 = x;
		x = tmp;
		diffuse(0, x, x0, diff, dt);
		// SWAP ( x0, x );
		tmp = x0;
		x0 = x;
		x = tmp;
		advect(0, x, x0, u, v, dt);
	}

	void vel_step(float[] u, float[] v, float[] u0, float[] v0, float visc,
			float dt) {
		add_source(u, u0, dt);
		add_source(v, v0, dt);

		float[] tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		
		diffuse(1, u, u0, visc, dt);
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		
		diffuse(2, v, v0, visc, dt);
		project(u, v, u0, v0);
		
		tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		
		advect(1, u, u0, u0, v0, dt);
		advect(2, v, v0, u0, v0, dt);
		project(u, v, u0, v0);
	}

	void project(float[] u, float[] v, float[] p, float[] div) {
		int i, j, k;
		float h;
		h = 1.0f / N;
		for (i = 1; i <= N; i++) {
			for (j = 1; j <= N; j++) {
				div[IX(i, j)] = -0.5f
						* h
						* (u[IX(i + 1, j)] - u[IX(i - 1, j)] + v[IX(i, j + 1)] - v[IX(
								i, j - 1)]);
				p[IX(i, j)] = 0;
			}
		}
		set_bnd(0, div);
		set_bnd(0, p);
		for (k = 0; k < 20; k++) {
			for (i = 1; i <= N; i++) {
				for (j = 1; j <= N; j++) {
					p[IX(i, j)] = (div[IX(i, j)] + p[IX(i - 1, j)]
							+ p[IX(i + 1, j)] + p[IX(i, j - 1)] + p[IX(i, j + 1)]) / 4;
				}
			}
			set_bnd(0, p);
		}
		for (i = 1; i <= N; i++) {
			for (j = 1; j <= N; j++) {
				u[IX(i, j)] -= 0.5 * (p[IX(i + 1, j)] - p[IX(i - 1, j)]) / h;
				v[IX(i, j)] -= 0.5 * (p[IX(i, j + 1)] - p[IX(i, j - 1)]) / h;
			}
		}
		set_bnd(1, u);
		set_bnd(2, v);
	}

	void add_source(float[] x, float[] s, float dt) {
		int i, size = (N + 2) * (N + 2);
		for (i = 0; i < size; i++)
			x[i] += dt * s[i];
	}

	public void step(float[] densPrev, float[] uPrev, float[] vPrev,
			float diff, float visc, float dt) {
		/*
		 * Diffuse stepper
		 */
		dens_prev = densPrev;
		u_prev = uPrev;
		v_prev = vPrev;
		vel_step(u, v, u_prev, v_prev, visc, dt);
		dens_step(dens, dens_prev, u, v, diff, dt);
	}

	private void diffuse(int b, float[] x, float[] x0, float diff, float dt) {
		int i, j, k;
		float a = dt * diff * N * N;
		for (k = 0; k < 20; k++) {
			for (i = 1; i <= N; i++) {
				for (j = 1; j <= N; j++) {
					x[IX(i, j)] = (x0[IX(i, j)] + a
							* (x[IX(i - 1, j)] + x[IX(i + 1, j)]
									+ x[IX(i, j - 1)] + x[IX(i, j + 1)]))
							/ (1 + 4 * a);
				}
			}
			set_bnd(b, x);
		}
	}

	void advect(int b, float[] d, float[] d0, float[] u, float[] v, float dt) {
		int i, j, i0, j0, i1, j1;
		float x, y, s0, t0, s1, t1, dt0;
		dt0 = dt * N;
		for (i = 1; i <= N; i++) {
			for (j = 1; j <= N; j++) {
				x = i - dt0 * u[IX(i, j)];
				y = j - dt0 * v[IX(i, j)];
				if (x < 0.5)
					x = 0.5f;
				if (x > N + 0.5)
					x = N + 0.5f;
				i0 = (int) x;
				i1 = i0 + 1;
				if (y < 0.5)
					y = 0.5f;
				if (y > N + 0.5)
					y = N + 0.5f;
				j0 = (int) y;
				j1 = j0 + 1;
				s1 = x - i0;
				s0 = 1 - s1;
				t1 = y - j0;
				t0 = 1 - t1;
				d[IX(i, j)] = s0 * (t0 * d0[IX(i0, j0)] + t1 * d0[IX(i0, j1)])
						+ s1 * (t0 * d0[IX(i1, j0)] + t1 * d0[IX(i1, j1)]);
			}
		}
		set_bnd(b, d);
	}

	/**
	 * This simply means that the horizontal component of the velocity should be
	 * zero on the vertical walls, while the vertical component of the velocity
	 * should be zero on the horizontal walls. For the density and other fields
	 * considered in the code we simply assume continuity
	 * 
	 * @param b
	 * @param x
	 */
	private void set_bnd(int b, float[] x) {
		int i;
		for (i = 1; i <= N; i++) {
			x[IX(0, i)] = b == 1 ? -x[IX(1, i)] : x[IX(1, i)];
			x[IX(N + 1, i)] = b == 1 ? -x[IX(N, i)] : x[IX(N, i)];
			x[IX(i, 0)] = b == 2 ? -x[IX(i, 1)] : x[IX(i, 1)];
			x[IX(i, N + 1)] = b == 2 ? -x[IX(i, N)] : x[IX(i, N)];
		}
		x[IX(0, 0)] = 0.5f * (x[IX(1, 0)] + x[IX(0, 1)]);
		x[IX(0, N + 1)] = 0.5f * (x[IX(1, N + 1)] + x[IX(0, N)]);
		x[IX(N + 1, 0)] = 0.5f * (x[IX(N, 0)] + x[IX(N + 1, 1)]);
		x[IX(N + 1, N + 1)] = 0.5f * (x[IX(N, N + 1)] + x[IX(N + 1, N)]);
	}

	public void evaporate(float f, float dt) {
		for (int i=0;i<dens.length;i++) {
			dens[i] -= f*dt;
			if (dens[i]<0) dens[i]=0;
		}
	}

}
