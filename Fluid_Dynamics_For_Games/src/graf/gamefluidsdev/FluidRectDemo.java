package graf.gamefluidsdev;

import graf.gamefluids.FluidRect;
import graf.gradient.ColorGradient;
import processing.core.PApplet;

public class FluidRectDemo extends PApplet {

	private static final long serialVersionUID = -6209751846308820317L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.gamefluidsdev.FluidRectDemo"});
	}
	
	ColorGradient gradient;// = ColorGradient.random(20);//FIRE;
	
	FluidRect fluid;
	
	int DRAW_WIDTH_MOUSE_DOWN = 3;
	int DRAW_WIDTH_MOUSE_UP = 5;
	float DRAW_STRENGTH = 0.8f;
	
	float DT = 1.0f;
	float DIFF = 0.00001f;
	float VISC = 0.0001f;
	float EVAPORATE_NORM =  0.0001f;
	float EVAPORATE_CLEAR = 0.01f;
	
	public void setup() {
		size(320,480,P3D);
		fluid = new FluidRect(80,120);
		background(0);
		loadPixels();
		//gradient = ColorGradient.random(7,true);//FIRE;
		gradient = ColorGradient.FIRE;
	}
	
	public void keyPressed() {
		if (key=='s') {
			save("output/FluidRectDemo_preview.jpg");
		} else if (key==' ') {
			setup();
		}
	}
	
	
	public void draw() {
		
		boolean addSmoke = (mousePressed&&mouseButton==LEFT);
		
		cursor(addSmoke?CROSS:HAND);//HAND);
		
		//ms[frameCount%ms.length] = millis();
		int ix = constrain(mouseX, 0, width-1)*fluid.WIDTH/width;
		int iy = constrain(mouseY, 0, height-1)*fluid.HEIGHT/height;
		
		float[] newDens = new float[fluid.size];
		float[] newU = new float[fluid.size];
		float[] newV = new float[fluid.size];
		
		int wid = addSmoke?DRAW_WIDTH_MOUSE_DOWN:DRAW_WIDTH_MOUSE_UP;
		float maxDist = sqrt(wid*wid + wid*wid);
		float totalCirclePressure=0;
		
		for (int xo=-wid;xo<=wid;xo++) {
			for (int yo=-wid;yo<=wid;yo++) {
				int xx = ix+xo;
				int yy = iy+yo;
				if (xx>0&&xx<fluid.WIDTH-1&&yy>0&&yy<fluid.HEIGHT-1) {
					float dToCen = sqrt(xo*xo+yo*yo);
					float pressure = (1-dToCen/maxDist);
					totalCirclePressure += pressure;
					if (addSmoke) {
						newDens[fluid.coordToIndex(xx, yy)] = pressure*DT*DRAW_STRENGTH;
					}
				}
			}
		}
		
		if (addSmoke) {
			float rd=-HALF_PI + random(-0.1f,0.1f);
			float spd = random(0.7f,1.0f);
			float vx = spd*cos(rd);
			float vy = spd*sin(rd);
			if (mouseX-pmouseX!=0 || mouseY-pmouseY!=0) {
				vx += 0.2f*(mouseX-pmouseX)*((float)fluid.WIDTH/width);
				vy += 0.2f*(mouseY-pmouseY)*((float)fluid.HEIGHT/height);
			}
			newU[fluid.coordToIndex(ix, iy)] = vx * DT;
			newV[fluid.coordToIndex(ix, iy)] = vy * DT;
		} 
		
		
		else {
			for (int xo=-wid;xo<=wid;xo++) {
				for (int yo=-wid;yo<=wid;yo++) {
					int xx = ix+xo;
					int yy = iy+yo;
					if (xx>0&&xx<fluid.WIDTH-1&&yy>0&&yy<fluid.HEIGHT-1) {
						float dToCen = sqrt(xo*xo+yo*yo);
						float pressure = (1-dToCen/maxDist)/totalCirclePressure;
						float vx=0, vy=0;
						if (addSmoke) {
							float rd=-HALF_PI + random(-0.4f,0.4f);
							float spd = random(0.7f,1.0f);
							vx = spd*cos(rd);
							vy = spd*sin(rd);
						} else {
							vx=0;
							vy=0;
						}
						if (mouseX-pmouseX!=0 || mouseY-pmouseY!=0) {
							vx += 0.5f*(mouseX-pmouseX)*((float)fluid.WIDTH/width);
							vy += 0.5f*(mouseY-pmouseY)*((float)fluid.HEIGHT/height);
						}
						newU[fluid.coordToIndex(xx, yy)] = pressure * vx * DT;
						newV[fluid.coordToIndex(xx, yy)] = pressure * vy * DT;
					}
				}
			}
		}

		
		
		
		//background(0);
		rectMode(CENTER);
		ellipseMode(RADIUS);
		//int xside = fluid.;
		float scaleX = (float)width/fluid.WIDTH;
		int sclx = (int)(scaleX*0.5);
		float scaleY = (float)height/fluid.HEIGHT;
		int scly = (int)(scaleY*0.5);
		//int ti = millis();
		
		for (int y=1;y<fluid.HEIGHT-1;y++) {
			float sy = scaleY*(0.5f+y);
			for (int x=1;x<fluid.WIDTH-1;x++) {
				float sx = scaleX*(0.5f+x);
			
				//stroke(255);
				//noStroke();
				
				int col = gradient.getColorAt(fluid.getDensity(x,y));
				//col = 0xb0000000 | (0xffffff&col);
				//fill(col);
				
				int isx = (int)sx;
				int isy = (int)sy;
				int ii = isx-sclx + width*(isy-scly);
				for (int yo=-scly;yo<=scly;yo++) {
					for (int xo=-sclx;xo<=sclx;xo++) {
						pixels[ii++]=col;
					}
					ii-=(sclx+sclx+1)+width;
				}
				
				//rect(sx,sy,scaleX,scaleY);
				/*stroke(255,0,0);
				float xo = scaleX *	 20 * fluid.getVelocityX(x,y);
				float yo = scaleY * 20 * fluid.getVelocityY(x,y);
				line(sx,sy,sx+xo,sy+yo);*/
			}
		}
		//println("draw: "+(millis()-ti));
		
		//ti = millis();
		fluid.step(newDens, newU, newV,	DIFF, VISC, DT);
		if (mousePressed&&mouseButton==RIGHT) {
			fluid.evaporate(EVAPORATE_CLEAR, DT);
		} else {
			fluid.evaporate(EVAPORATE_NORM, DT);
		}
		//ti = millis()-ti;
		
		//println("solve: "+ti);//nf(av,1,2));
		//println(frameRate);
		
		//saveFrame("./output2/FluidRectDemo_####.bmp");
	}
	
	//float interpolate()

}
