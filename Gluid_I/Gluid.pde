class Gluid {

  private float[][] density;
  private float[][] newDensity;

  Gluid(int w, int h) {
    density = new float[w][h];
    newDensity = new float[w][h];
    for (int x=0;x<w;x++) for (int y=0;y<h;y++) {
      density[x][y]=0;
      newDensity[x][y]=0;
    }
  }

  void addDensityAt(int x, int y) {
    float p = 30.0;
    int rad = 30;
    //float maxDist = sqrt(rad*rad + rad*rad);
    //x = ((x<0?0:x)>density.length-1)?(density.length-1):x;
    //y = ((y<0?0:y)>density[0].length-1)?(density[0].length-1):y;
    if (x<0) x=0;
    if (x>density.length-1) x=density.length-1;
    if (y<0) y=0;
    if (y>density[0].length-1) y=density[0].length-1;
    density[x][y] += p;

    for (int xo=-rad;xo<=rad;xo++) {
      for (int yo=-rad;yo<=rad;yo++) {
        float d = sqrt(xo*xo+yo*yo);
        if (d<=rad&&d>0) {
          float noD = 1.0-sqrt(xo*xo+yo*yo)/rad;
          int nx = x+xo;
          int ny = y+yo;
          if (nx>=0&&nx<density.length&&ny>=0&&ny<density[0].length) {
            density[nx][ny] += p*noD;
          }
        }
      }
    }

    for (int xx=0;xx<density.length;xx++) for (int yy=0;yy<density[0].length;yy++) newDensity[xx][yy] = density[xx][yy];

  }

  void dissolve() {
    for (int x=0;x<density.length;x++) {
      for (int y=0;y<density[0].length;y++) {
        float lossFac = 0.03;
        float dLoss = density[x][y]*lossFac;
        boolean xMinOk = (x>0);
        boolean xMaxOk = (x<density.length-1);
        boolean yMinOk = (y>0);
        boolean yMaxOk = (y<density[0].length-1);
        float d = density[x][y];
        if (xMinOk) if (d>density[x-1][y] && newDensity[x-1][y]<255-dLoss) {
          newDensity[x][y]-=dLoss;
          newDensity[x-1][y]+=dLoss;
        }
        if (xMaxOk) if (d>density[x+1][y] && newDensity[x+1][y]<255-dLoss) {
          newDensity[x][y]-=dLoss;
          newDensity[x+1][y]+=dLoss;
        }
        if (yMinOk) if (d>density[x][y-1] && newDensity[x][y-1]<255-dLoss) {
          newDensity[x][y]-=dLoss;
          newDensity[x][y-1]+=dLoss;
        }
        if (yMaxOk) if (d>density[x][y+1] && newDensity[x][y+1]<255-dLoss) {
          newDensity[x][y]-=dLoss;
          newDensity[x][y+1]+=dLoss;
        }
        if (xMinOk&&yMinOk) if (d>density[x-1][y-1] && newDensity[x-1][y-1]<255-dLoss*0.7) {
          newDensity[x][y]-=dLoss*0.7;
          newDensity[x-1][y-1]+=dLoss*0.7;
        }
        if (xMinOk&&yMaxOk) if (d>density[x-1][y+1] && newDensity[x-1][y+1]<255-dLoss*0.7) {
          newDensity[x][y]-=dLoss*0.7;
          newDensity[x-1][y+1]+=dLoss*0.7;
        }
        if (xMaxOk&&yMinOk) if (d>density[x+1][y-1] && newDensity[x+1][y-1]<255-dLoss*0.7) {
          newDensity[x][y]-=dLoss*0.7;
          newDensity[x+1][y-1]+=dLoss*0.7;
        }
        if (xMaxOk&&yMaxOk) if (d>density[x+1][y+1] && newDensity[x+1][y+1]<255-dLoss*0.7) {
          newDensity[x][y]-=dLoss*0.7;
          newDensity[x+1][y+1]+=dLoss*0.7;
        }
      }
    }

    for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) density[x][y] = newDensity[x][y];
  }


  void listenTo(WindField wf) {
    for (int x=0;x<density.length;x++) {
      for (int y=0;y<density[0].length;y++) {
        float[] xy = wf.getWindAt(x,y);
        int nx = int(x+xy[0]);
        int ny = int(y+xy[1]);
        if (nx>=0&&nx<density.length&&ny>=0&&ny<density[0].length) {
          float lossFac = 0.2;
          float dLoss = density[x][y]*lossFac;
          newDensity[x][y]-=dLoss;
          newDensity[nx][ny]+=dLoss;
        }
      }
    }
    
    for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) density[x][y] = newDensity[x][y];
  }

  void draw() {
    for (int x=0;x<density.length;x++) {
      for (int y=0;y<density[0].length;y++) {
        set(x,y,color(density[x][y]));
      }
    }
  }

}
