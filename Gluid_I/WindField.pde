class WindField {

  private Wind[][] wind;
  private int step;

  WindField(int w, int h, int _step) {
    step = _step;
    wind = new Wind[(int)(w/step)][(int)(h/step)];
    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {
        wind[x][y] = new Wind((x+0.5)*step,(y+0.5)*step);
      }
    }
  }


  void randomize() {
    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {
        wind[x][y].addNewXWind(random(-0.5,0.5));
        wind[x][y].addNewYWind(random(-0.5,0.5));
      }
    }
    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {
        wind[x][y].updateNewWind();
      }
    }
  }
  
  
  void dissolve() {
    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {

        float xAdd = 0;
        float yAdd = 0;
        float neiAmount = 0;
        for (int xo=-1;xo<=1;xo++) {
          for (int yo=-1;yo<=1;yo++) {
            int nx = x+xo;
            int ny = y+yo;
            if (nx>=0&&nx<wind.length&&ny>=0&&ny<wind[0].length&&(!(nx==0&&ny==0))) {
              float xNei = wind[nx][ny].getXWind();
              float yNei = wind[nx][ny].getYWind();
              xAdd += xNei;
              yAdd += yNei;
              wind[nx][ny].addNewXWind(xNei*-0.01);
              wind[nx][ny].addNewYWind(yNei*-0.01);
              neiAmount++;
            }
          }
        }
        xAdd /= neiAmount;
        yAdd /= neiAmount;
        wind[x][y].addNewXWind(xAdd*0.05);
        wind[x][y].addNewYWind(yAdd*0.05);
        
        //wind[x][y].addNewXWind(xAdd*0.1);
        //wind[x][y].addNewYWind(yAdd*0.1);
        //wind[x][y].draw();
      }
    }

    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {
        wind[x][y].updateNewWind();
      }
    }

  }
  
  
  void listenTo(float _x, float _y, float _px, float _py) {
    if (_x<0) _x=0;
    if (_x>width-1) _x=width-1;
    //_x = (_x<0?0:_x)>=width?width-1:_x;
    if (_y<0) _y=0;
    if (_y>height-1) _y=height-1;
    //_y = (_y<0?0:_y)>=height?height-1:_y;
    
    if (_px<0) _px=0;
    if (_px>width-1) _px=width-1;
    if (_py<0) _py=0;
    if (_py>height-1) _py=height-1;
    
    //_px = (_px<0?0:_px)>=width?width-1:_px;
    //_py = (_py<0?0:_py)>=height?height-1:_py;
    int ix = (int)floor(_x/step - 0.5);
    int iy = (int)floor(_y/step - 0.5);
    float dx=-_px+_x;
    float dy=-_py+_y;
    dx*=0.5;
    dy*=0.5;
    if (ix>=0&&iy>=0) {
      wind[ix][iy].addNewXWind(dx);
      wind[ix][iy].addNewYWind(dy);
    }
    if (ix<wind.length-1&&iy>=0) {
      wind[ix+1][iy].addNewXWind(dx);
      wind[ix+1][iy].addNewYWind(dy);
    }
    if (ix>=0&&iy<wind[0].length-1) {
      wind[ix][iy+1].addNewXWind(dx);
      wind[ix][iy+1].addNewYWind(dy);
    }
    if (ix<wind.length-1&&iy<wind[0].length-1) {
      wind[ix+1][iy+1].addNewXWind(dx);
      wind[ix+1][iy+1].addNewYWind(dy);
    }
  }



  float[] getWindAt(int _x, int _y) {
    float[] xy = new float[2];
    
    int ix = (int)floor(_x/step - 0.5);
    int iy = (int)floor(_y/step - 0.5);
    float xAdd=0;
    float yAdd=0;
    float wDiv=0;
    float dx=0,dy=0;
    if (ix>=0&&iy>=0) {
      dx = wind[ix][iy].getXPos()-_x;
      dy = wind[ix][iy].getYPos()-_y;
      xAdd += wind[ix][iy].getXWind();
      yAdd += wind[ix][iy].getYWind();
      wDiv++;
    }
    if (ix<wind.length-1&&iy>=0) {
      dx = wind[ix+1][iy].getXPos()-_x;
      dy = wind[ix+1][iy].getYPos()-_y;
      xAdd += wind[ix+1][iy].getXWind();
      yAdd += wind[ix+1][iy].getYWind();
      wDiv++;
    }
    if (ix>=0&&iy<wind[0].length-1) {
      dx = wind[ix][iy+1].getXPos()-_x;
      dy = wind[ix][iy+1].getYPos()-_y;
      xAdd += wind[ix][iy+1].getXWind();
      yAdd += wind[ix][iy+1].getYWind();
      wDiv++;
    }
    if (ix<wind.length-1&&iy<wind[0].length-1) {
      dx = wind[ix+1][iy+1].getXPos()-_x;
      dy = wind[ix+1][iy+1].getYPos()-_y;
      xAdd += wind[ix+1][iy+1].getXWind();
      yAdd += wind[ix+1][iy+1].getYWind();
      wDiv++;
    }
    if (wDiv!=0) {
      xAdd/=wDiv;
      yAdd/=wDiv;
    }
    xy[0]=xAdd;
    xy[1]=yAdd;
    
    return xy;
  }



  void draw() {
    for (int x=0;x<wind.length;x++) {
      for (int y=0;y<wind[0].length;y++) {
        wind[x][y].draw();
      }
    }
  }

}
