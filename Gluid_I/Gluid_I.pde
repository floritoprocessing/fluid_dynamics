Gluid gluid;
WindField windField;

void setup() {
  size(200,300,P3D);
  gluid = new Gluid(width,height);
  windField = new WindField(width,height,10);
}

void draw() {
  background(0,0,0);
  if (mousePressed) {
    gluid.addDensityAt(mouseX,mouseY);
  }
  //windField.randomize();
  windField.dissolve();
  gluid.dissolve();
  if (mousePressed) windField.listenTo(mouseX,mouseY,pmouseX,pmouseY);
  gluid.listenTo(windField);
  gluid.draw();
  //windField.draw();
}
