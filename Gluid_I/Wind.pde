class Wind {
  
  private float x = 0, y = 0;
  private float xw = 0, yw = 0;
  private float nxw = 0, nyw = 0;
  private float TARGET_STRENGTH = 5.0;
  
  Wind(float _x, float _y) {
    x = _x;
    y = _y;
    float rd = random(TWO_PI);
    float s = random(3.0,8.0);
    xw = s*cos(rd);
    yw = s*sin(rd);
    nxw = xw;
    nyw = yw;
  }
  
  float getXPos() {
    return x;
  }
  float getYPos() {
    return y;
  }
  
  float getXWind() {
    return xw;
  }
  float getYWind() {
    return yw;
  }
  void addNewXWind(float _nx) {
    nxw += _nx;
  }
  void addNewYWind(float _ny) {
    nyw += _ny;
  }
  
  void updateNewWind() {
    float rd=atan2(nyw,nxw);
    float s=sqrt(nxw*nxw+nyw*nyw);
    //if (s>TARGET_STRENGTH) 
    s = (5.0*s + 1.0*TARGET_STRENGTH)/6.0;
    nxw = s*cos(rd);
    nyw = s*sin(rd);
    xw = nxw;
    yw = nyw;
  }
  
    
  void draw() {
    stroke(255,0,0);
    line(x,y,x+xw,y+yw);
  }
}
