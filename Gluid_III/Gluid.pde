class Gluid {

  private float[] density;
  private float[] newDensity;
  private float DENSITY_MAX = 255;
  private int step;
  private int w, h;

  Gluid(int w, int h, int _step) {
    this.w=w/_step;
    this.h=h/_step;
    step = _step;
    density = new float[(w/step)*(h/step)];
    newDensity = new float[(w/step)*(h/step)];
    int i=0;
    for (int x=0;x<w/step;x++) for (int y=0;y<h/step;y++) {
      density[i]=random(DENSITY_MAX);
      newDensity[i++]=random(DENSITY_MAX);
    }
  }

  void updateDensity() {
    //for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) {
    System.arraycopy(newDensity,0,density,0,newDensity.length);  
    //density[x][y] = newDensity[x][y];
    //if (density[x][y]<0) density[x][y]=0;
    //if (density[x][y]>255) density[x][y]=255;
    //}
  }

  void addDensityAt(float _x, float _y, boolean drawIt) {
    int x=(int)_x;
    int y=(int)_y;
    x/=step;
    y/=step;
    float p = 2.0;
    int rad = 40/step;
    if (x<0) x=0;
    if (x>=w) x=w-1;
    if (y<0) y=0;
    if (y>=h) y=h-1;

    int ii = y*w+x;

    density[ii] += p;
    if (density[ii]>DENSITY_MAX) density[ii]=DENSITY_MAX;

    
    for (int xo=-rad;xo<=rad;xo++) {
      for (int yo=-rad;yo<=rad;yo++) {
        float d = sqrt(xo*xo+yo*yo);
        if (d<=rad&&d>0) {
          float noD = 1.0-d/rad;//sqrt(xo*xo+yo*yo)
          int nx = x+xo;
          int ny = y+yo;
          if (nx>=0&&nx<w&&ny>=0&&ny<h) {
            int i=nx + ny*w;
            if (drawIt)
              newDensity[i] += p*noD;
            else {
              newDensity[i] -= p*noD;
              newDensity[i] = max(0,newDensity[i]);
            } 
          }
        }
      }
    }
    updateDensity();
  }

  void evaporate(float p) {
    //for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) 
    for (int i=0;i<density.length;i++)
      if (newDensity[i]>p) newDensity[i]-=p;
  }

  void dissolve() {

    //for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) {
    int x=0,y=0;  
    for (int i=0;i<density.length;i++) {


      for (int xo=-1;xo<=1;xo+=2) {
        int nx=x+xo;
        if (nx>=0&&nx<w) {
          float loss=(density[i]-density[nx+y*w])*0.2;
          newDensity[i]-=loss;
        }
      }
      for (int yo=-1;yo<=1;yo+=2) {
        int ny=y+yo;
        if (ny>=0&&ny<h) {
          float loss=(density[i]-density[x+ny*w])*0.2;
          newDensity[i]-=loss;
        }
      }
      if (++x==w) {
        x=0;
        y++;
      }

    }

    updateDensity();

  }


  void draw() {
    colorMode(HSB,255);
    int x=0, y=0;
    for (int i=0;i<density.length;i++) {

      //for (int x=0;x<density.length;x++) {
      //for (int y=0;y<density[0].length;y++) {
      for (int xo=0;xo<step;xo++) for (int yo=0;yo<step;yo++) {
        set(x*step+xo,y*step+yo,color(170-0.1*density[i],255-0.8*density[i],density[i]));
      }
      if (++x==w) {
        x=0;
        y++;
      }
    }
    //}
  }

}

