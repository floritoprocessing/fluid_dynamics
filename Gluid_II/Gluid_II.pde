Gluid gluid;

void setup() {
  size(500,300,P3D);
  gluid = new Gluid(width,height,5);
}

void draw() {
  background(0,0,0);
  if (mousePressed) {
    float dx = pmouseX-mouseX;
    float dy = pmouseY-mouseY;
    float d = sqrt(dx*dx+dy*dy);
    for (float i=0;i<d;i++) {
      gluid.addDensityAt(mouseX+i/d*dx,mouseY+i/d*dy);
    }
    
  }
  gluid.evaporate(0.02);
  gluid.dissolve();
  gluid.draw();
}
