class Gluid {

  private float[][] density;
  private float[][] newDensity;
  private float DENSITY_MAX = 255;
  private int step;
  

  Gluid(int w, int h, int _step) {
    step = _step;
    density = new float[w/step][h/step];
    newDensity = new float[w/step][h/step];
    for (int x=0;x<w/step;x++) for (int y=0;y<h/step;y++) {
      density[x][y]=0;
      newDensity[x][y]=0;
    }
  }
  
  void updateDensity() {
    for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) {
      density[x][y] = newDensity[x][y];
      //if (density[x][y]<0) density[x][y]=0;
      //if (density[x][y]>255) density[x][y]=255;
    }
  }

  void addDensityAt(float _x, float _y) {
    int x=(int)_x;
    int y=(int)_y;
    x/=step;
    y/=step;
    float p = 2.0;
    int rad = 40/step;
    if (x<0) x=0;
    if (x>density.length-1) x=density.length-1;
    if (y<0) y=0;
    if (y>density[0].length-1) y=density[0].length-1;
    density[x][y] += p;
    if (density[x][y]>DENSITY_MAX) density[x][y]=DENSITY_MAX;

    for (int xo=-rad;xo<=rad;xo++) {
      for (int yo=-rad;yo<=rad;yo++) {
        float d = sqrt(xo*xo+yo*yo);
        if (d<=rad&&d>0) {
          float noD = 1.0-sqrt(xo*xo+yo*yo)/rad;
          int nx = x+xo;
          int ny = y+yo;
          if (nx>=0&&nx<density.length&&ny>=0&&ny<density[0].length) {
            newDensity[nx][ny] += p*noD;
          }
        }
      }
    }
    updateDensity();
  }
  
  void evaporate(float p) {
    for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) if (newDensity[x][y]>p) newDensity[x][y]-=p;
  }

  void dissolve() {

      for (int x=0;x<density.length;x++) for (int y=0;y<density[0].length;y++) {
         
        for (int xo=-1;xo<=1;xo+=2) {
          int nx=x+xo;
          if (nx>=0&&nx<density.length) {
            float loss=(density[x][y]-density[nx][y])*0.2;
            newDensity[x][y]-=loss;
          }
        }
        for (int yo=-1;yo<=1;yo+=2) {
          int ny=y+yo;
          if (ny>=0&&ny<density[0].length) {
            float loss=(density[x][y]-density[x][ny])*0.2;
            newDensity[x][y]-=loss;
          }
        }
      }
      
      updateDensity();

  }


  void draw() {
    colorMode(HSB,255);
    for (int x=0;x<density.length;x++) {
      for (int y=0;y<density[0].length;y++) {
        for (int xo=0;xo<step;xo++) for (int yo=0;yo<step;yo++) {
          set(x*step+xo,y*step+yo,color(170-0.1*density[x][y],255-0.8*density[x][y],density[x][y]));
        }
      }
    }
  }

}
