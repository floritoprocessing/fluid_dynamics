

set tcl_precision 12
set pi 3.141592654

set DEFAULT_CANVAS_WIDTH 320
set DEFAULT_CANVAS_HEIGHT 240

set canvasWidth 0
set canvasHeight 0

set currentIndex 0

set infoArray(polygonCount) 0
set infoArray(frameCount) 0
set infoArray(Merge) 0
set pointArray() {} 

# size of the markers
set gPointSize 5

# radius used when initialising a polygon
set gRadius 20

set gPoly 0
set gPoint 0

# offset for text labels
set textOffsetX 8
set textOffsetY 0

set colorList {red blue white green yellow orange skyblue firebrick goldenrod}
set gColor red

set gWindingThreshold 6

set gProgress 0

set filelist ""
set saveDirectory "./"
set gPolygonFileName ""


