/*
 * vector2D.c
 *
 */

#include <iostream>
#include <math.h>
#include "vector2D.h"


Vector2D::Vector2D(double this_x, double this_y) {
  x = this_x;
  y = this_y;

}

Vector2D::Vector2D() {

}


/*
 * VectorLength
 *
 *  -- return the length of a 2D vector
 */

double Vector2D::VectorLength () {

  return sqrt((x*x)+(y*y));

}


/*
 * DotProduct
 *
 *  -- return the dot product of two 2D vectors
 */

double DotProduct (Vector2D U, Vector2D V) {

  return U.getx()*V.getx() + U.gety()*V.gety();

}


/*
 * VectorAngle
 *
 *  -- return the angle between two 2D vectors
 */

double VectorAngle (Vector2D U, Vector2D V) {
 
  int sign;
  double divisor = U.VectorLength()*V.VectorLength();
  if (divisor == 0) return 0;
  double temp = DotProduct(U,V)/divisor;
  if (temp > 1) temp = 1;
  if (temp < -1) temp = -1;
  if (((U.getx() * V.gety()) - (U.gety() * V.getx())) < 0) {
    sign = -1;
  } else {
    sign = 1;
  }
  return sign * acos(temp);

}

