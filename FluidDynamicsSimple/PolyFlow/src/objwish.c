/*****************************************************************************
 *                                                                           *
 * objwish.cc                                                                *
 *                                                                           *
 * Steven Mills                                                              *
 *                                                                           *
 * 23rd December 1998                                                        *
 *                                                                           *
 * This is the main file for making a new version of Tcl/Tk which includes a *
 * few extra functions used by my PhD project. The functions are to read,    *
 * compute, and display the objects, as well as some book-keeping.           *
 *                                                                           *
 * Hacked by Dion for his own evil needs.                                    *
 *                                                                           *
 *****************************************************************************/

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <tcl.h>
#include <tk.h>

//////////////////////////////////////
//   Global Variables for Tcl/Tk    //
//////////////////////////////////////

// globals go here

// The following variables are linked to Tcl variables by the function
// link_variables, called from Tcl as C_Link_Variables.

// link vars go here

//////////////////////////////////////
// Prototypes for new TCL functions //
//////////////////////////////////////


// C_Link_Variables

// Implementations are all in tcl_functions.cc
#include "flow.h"


////////////////////////////////
//   Tcl/Tk initialisation    //
////////////////////////////////

int my_init( Tcl_Interp *interp )
{
  // Initialise Tcl, Tk and ToGL

  if (Tcl_Init(interp) == TCL_ERROR) {
    return TCL_ERROR;
  }

  if (Tk_Init(interp) == TCL_ERROR) {
    return TCL_ERROR;
  }

  // Create application specific commands
  
  Tcl_CreateCommand( interp, "C_DoFlowCalculation", DoFlowCalculation,
		     (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL );

 
   return TCL_OK;
}

int main( int argc, char *argv[] )
{
  Tk_Main( argc, argv, my_init );
  return 0;
}
