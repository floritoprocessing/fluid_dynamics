package graf.gamefluidsdev;

import processing.core.PApplet;

public class FluidSquareDemo extends PApplet {

	private static final long serialVersionUID = -6209751846308820317L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.gamefluidsdev.FluidSquareDemo"});
	}
	
	FluidSquare fluid;
	
	int DRAW_WIDTH = 2;
	
	float DT = 0.5f;
	float DIFF = 0.00001f;
	float VISC = 0.0001f;
	float EVAPORATE = 0.004f;
	
	public void setup() {
		size(400,400,P3D);
		fluid = new FluidSquare(40);
	}
	
	public void keyPressed() {
		setup();
	}
	
	public void draw() {
		
		int ix = constrain(mouseX, 0, width-1)*fluid.N2/width;
		int iy = constrain(mouseY, 0, height-1)*fluid.N2/height;
		
		float[] newDens = new float[fluid.size];
		float[] newU = new float[fluid.size];
		float[] newV = new float[fluid.size];
		
		if (mousePressed) {
			float maxDist = sqrt(DRAW_WIDTH*DRAW_WIDTH + DRAW_WIDTH*DRAW_WIDTH);
			for (int xo=-DRAW_WIDTH;xo<=DRAW_WIDTH;xo++) {
				for (int yo=-DRAW_WIDTH;yo<=DRAW_WIDTH;yo++) {
					float dToCen = sqrt(xo*xo+yo*yo);
					int xx = ix+xo;
					int yy = iy+yo;
					if (xx>0&&xx<fluid.N2-1&&yy>0&&yy<fluid.N2-1) {
						newDens[fluid.IX(xx, yy)] = (1-dToCen/maxDist)*DT;
						if (mouseX-pmouseX!=0 || mouseY-pmouseY!=0) {
							float vx = (mouseX-pmouseX)*((float)fluid.N2/width);
							float vy = (mouseY-pmouseY)*((float)fluid.N2/height);
							newU[fluid.IX(ix, iy)] = vx * DT;
							newV[fluid.IX(ix, iy)] = vy * DT;
						}
					}
				}
			}
		}
		
		
		
		background(0);
		rectMode(CENTER);
		int side = fluid.N2;
		float screenScale = (float)width/side;
		for (int x=0;x<side;x++) {
			float sx = screenScale*(0.5f+x);
			for (int y=0;y<side;y++) {
				float sy = screenScale*(0.5f+y);
				stroke(255,255,255,64);
				fill(fluid.getDensity(x,y)*255);
				rect(sx,sy,screenScale,screenScale);
				stroke(255,0,0);
				float xo = screenScale * 20 * fluid.getVelocityX(x,y);
				float yo = screenScale * 20 * fluid.getVelocityY(x,y);
				line(sx,sy,sx+xo,sy+yo);
			}
		}
		
		fluid.step(newDens, newU, newV,	DIFF, VISC, DT);
		fluid.evaporate(EVAPORATE, DT);
	}

}
